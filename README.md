# Sistema de Eleição

O sistema de Eleição é um programa de computador que funciona por meio de um servidor web utilizando tecnologias modernas, gratuitas e seguras, através de uma linguagem de programação e de um banco de dados local para permitir o registro confiável e anônimo de votos, bem como sua contagem/apuração após o encerramento das seções eleitorais;

O acesso administrativo do mesário ao sistema é feito através de um login (o CPF do usuário) e uma senha (encaminhada ao e-mail pessoal);

Para a votação são necessários pelo menos 02 computadores: um para o Mesário e outro para os eleitores votarem;

## Tecnologias utilizadas
- CakePHP
- PHP 5.6+
- Posgres 9+
- Apache2+