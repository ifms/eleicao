<?php
/* 
 * Classe que provê a autenticação via Active Diretory
 */

namespace App\Auth;

use Cake\Auth\BaseAuthenticate;
use Cake\Http\ServerRequest;
use Cake\Http\Response;
use Cake\ORM\TableRegistry;

class AdAuthenticate extends BaseAuthenticate
{
    public function authenticate(ServerRequest $request, Response $response)
    {
        if (!empty($request->data) AND isset($request->data['siape']) AND isset($request->data['senha']))
        {
            $adServer = "ldap://10.1.0.11";
	
            $ldap = ldap_connect($adServer) or die('Não foi possível conectar ao servidor de autenticação AD');
            $username = $request->data['siape'];
            $password = $request->data['senha'];
            $ldaprdn = "ifms\\" . $username;

            ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);

            $bind = @ldap_bind($ldap, $ldaprdn, $password);

            if ($bind)
            {
                $filter = "(sAMAccountName=$username)";
                $result = ldap_search($ldap, "dc=ifms,dc=edu,dc=br", $filter);
                ldap_sort($ldap, $result, "sn");
                $info = ldap_get_entries($ldap, $result);

                for ($i = 0; $i < $info["count"]; $i++)
                {
                    if ($info['count'] > 1)
                        break;

                    // 'Nome: ' . $info[$i]["givenname"][0] . '<br/>';
                    // 'Nome completo: ' . $info[$i]["givenname"][0] . ' ' . $info[$i]["sn"][0] . '<br/>';
                    // 'SIAPE: ' . $info[$i]["samaccountname"][0] ."<br/>";
                    // 'Email: ' . $info[$i]["mail"][0] . '<br/>';
                    // 'CPF: ' . $info[$i]["extensionattribute6"][0] . '<br/>';
                    // 'Foto: ' . '<img src="data:image/jpeg;base64, ' . base64_encode($info[$i]["thumbnailphoto"][0]) . '" width="50"/></p>';

                    if (!empty($info[$i]["extensionattribute6"][0]))
                    {
                        $usuario_model = TableRegistry::get('Usuarios');

                        $usuario = $usuario_model->find('all', [
                            'conditions' => [
                                'cpf' => $info[$i]["extensionattribute6"][0]
                            ]
                        ]);

                        if (!empty($usuario->toArray()))
                        {
                            $usuario = $usuario->toArray()[0];
                            unset($usuario['senha']);
                            $usuario['foto'] = base64_encode($info[$i]["thumbnailphoto"][0]);
                            $usuario['siape'] = $info[$i]["samaccountname"][0];

                            return $usuario->toArray();
                        }
                        else
                            return -1; // CPF não encontrado
                    }
                }

                ldap_close($ldap);
            }
            else
                return false;
        }
        
        return false; // Se não foi possível validar
    }
}