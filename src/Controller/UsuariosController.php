<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Collection\Collection;
use Cake\Mailer\Email;

/**
 * Usuarios Controller
 *
 * @property \App\Model\Table\UsuariosTable $Usuarios
 */
class UsuariosController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // Allow users login and logout.
        $this->Auth->allow(['login', 'loginAd', 'logout']);
    }

    public function login()
    {
        $user = $this->Auth->user();
        // Verifico se usuário já está logado
        if (!empty($user))
        {
            // Se for mesário, redireciona para a página dele
            if ($user['grupo_id'] == 2)
                return $this->redirect(['controller' => 'Usuarios', 'action' => 'liberar_eleitor']);

            return $this->redirect($this->Auth->redirectUrl());
        }
        elseif ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                $this->Flash->success('Login efetuado com sucesso.');

                // Registrar login
                $this->Usuarios->Auditorias->add(['acao' => 'Login', 'usuario_id' => $user['id'], 'secao_id' => $user['secao_id']]);

                // Se for mesário, redireciona para a página dele
                if ($user['grupo_id'] == 2)
                    return $this->redirect(['controller' => 'Usuarios', 'action' => 'liberar_eleitor']);

                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error('CPF ou Senha errado(s). Tente novamente.');
        }

        $this->render('loginAd');
    }

    /*
     * Implementação de função de login utilizando o Active Directory - AD IP: 10.1.1.11 e 10.1.1.16 (usuário entra com SIAPE e Senha)
     */
    public function loginAd()
    {
        $user = $this->Auth->user();

        // Verifico se usuário já está logado
        if (!empty($user))
        {
            // Se for mesário, redireciona para a página dele
            if ($user['grupo_id'] == 2)
                return $this->redirect(['controller' => 'Usuarios', 'action' => 'liberar_eleitor']);

            return $this->redirect($this->Auth->redirectUrl());
        }
        elseif ($this->request->is('post')) {
            $user = $this->Auth->identify();

            if ($user) {
                $this->Auth->setUser($user);
                
                $this->Flash->success('Login efetuado com sucesso.');

                // Registrar login
                $this->Usuarios->Auditorias->add(['acao' => 'Login', 'usuario_id' => $user['id'], 'secao_id' => $user['secao_id']]);

                // Se for mesário, redireciona para a página dele
                if ($user['grupo_id'] == 2)
                    return $this->redirect(['controller' => 'Usuarios', 'action' => 'liberar_eleitor']);

                return $this->redirect($this->Auth->redirectUrl());
            }
            else
            {
                sleep(2); // Aguardamos 2 segundos - Prevenção de ataques de força bruta
                $this->Flash->error('SIAPE ou Senha errado(s). Tente novamente.');
            }
        }
    }

    public function logout()
    {
        $this->Flash->success('Usuário deslogado com sucesso.');
        $user = $this->Auth->user();
        if (!empty($user))
            $this->Usuarios->Auditorias->add(['acao' => 'Logout', 'usuario_id' => $user['id'], 'secao_id' => $user['secao_id']]);

        return $this->redirect($this->Auth->logout());
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Tipos', 'Grupos', 'Secoes', 'SecaoTrabalho', 'Status']
        ];
        $this->set('usuarios', $this->paginate($this->Usuarios));
        $this->set('_serialize', ['usuarios']);
    }

    /**
     * View method
     *
     * @param string|null $id Usuario id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $usuario = $this->Usuarios->get($id, [
            'contain' => ['Tipos', 'Grupos', 'Secoes', 'Status', 'Auditorias']
        ]);
        // Reordeno as auditorias pela data do registro descendentemente
        $auditorias = new Collection($usuario->auditorias);
        $auditorias = $auditorias->sortBy('registro', SORT_DESC);
        $usuario->auditorias = $auditorias->toArray();

        $this->set('usuario', $usuario);
        $this->set('_serialize', ['usuario']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $usuario = $this->Usuarios->newEntity();
        if ($this->request->is('post')) {
            $usuario = $this->Usuarios->patchEntity($usuario, $this->request->data);
            if ($this->Usuarios->save($usuario)) {
                $this->Flash->success(__('The usuario has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The usuario could not be saved. Please, try again.'));
            }
        }
        $tipos = $this->Usuarios->Tipos->find('list', ['limit' => 200]);
        $grupos = $this->Usuarios->Grupos->find('list', ['limit' => 200]);
        $secoes = $this->Usuarios->Secoes->find('list', ['limit' => 200]);
        $status = $this->Usuarios->Status->find('list', ['limit' => 200]);
        $this->set(compact('usuario', 'tipos', 'grupos', 'secoes', 'status'));
        $this->set('_serialize', ['usuario']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Usuario id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $usuario = $this->Usuarios->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $usuario = $this->Usuarios->patchEntity($usuario, $this->request->data);

            if ($this->Usuarios->save($usuario)) {
                $this->Flash->success(__('The usuario has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The usuario could not be saved. Please, try again.'));
            }
        }
        $tipos = $this->Usuarios->Tipos->find('list', ['limit' => 200]);
        $grupos = $this->Usuarios->Grupos->find('list', ['limit' => 200]);
        $secoes = $this->Usuarios->Secoes->find('list', ['limit' => 200]);
        $status = $this->Usuarios->Status->find('list', ['limit' => 200]);
        $this->set(compact('usuario', 'tipos', 'grupos', 'secoes', 'status'));
        $this->set('_serialize', ['usuario']);
    }

    public function resetSenha($id = null) {
        $usuario = $this->Usuarios->get($id, [
            'contain' => []
        ]);

        if (!empty($usuario->email)) 
        {
            //debug($usuario);

            // Gerar uma senha de 06 digitos
            $lmai = 'ABCDEFGHIJKLMNPQRSTUVWXYZ';
            $num = '1234567890';
            $senha = '';
            $caracteres = '';
            $caracteres .= $lmai;
            $caracteres .= $num;
            
            $len = strlen($caracteres);
            for ($n = 1; $n <= 6; $n++) {
                $rand = mt_rand(1, $len);
                $senha .= $caracteres[$rand-1];
            }

            $usuario = $this->Usuarios->patchEntity($usuario, ['senha' => $senha]);

            // Salvar nova senha do usuário
            if ($this->Usuarios->save($usuario)) {
            
                // Email com reset da senha
                $email = new Email('default');
                $email->to($usuario->email)
                    ->emailFormat('html')
                    ->subject('Reset de Senha para acesso ao Sistema - ' . parent::configuracao('titulo_site'));

                $mensagem = 'Olá, seus dados para acesso ao sistema são:<br/>' . 
                            'Endereço: ' . parent::configuracao('url') . '<br/>' .
                            'CPF: ' . $usuario->cpf . '<br/>' .
                            'Senha: ' . $senha . '<br/><br/>' .
                            'Ats,<br/><br/>' .
                            'Sistema - ' . parent::configuracao('titulo_site');

                if ($email->send($mensagem)) 
                {
                    $this->Flash->success('Senha resetada e encaminhada para o email do usuário com sucesso.');
                    $this->redirect($this->referer());
                }
                else
                    $this->Flash->error('Erro ao tentar encaminhar email com reset de senha. Tente novamente.');
            }
            else
                $this->Flash->error('Erro ao salvar o reset de senha do usuário.');
        }
        else
            $this->Flash->error('Usuário sem e-mail.');
    }

    /**
     * Delete method
     *
     * @param string|null $id Usuario id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $usuario = $this->Usuarios->get($id);
        if ($this->Usuarios->delete($usuario)) {
            $this->Flash->success(__('The usuario has been deleted.'));
        } else {
            $this->Flash->error(__('The usuario could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    /*
     *  Função onde o mesário libera o eleitor para votar
     */   
    public function liberarEleitor()
    {
        $usuario = $this->Usuarios->newEntity();
        $user = $this->Auth->user();
        
        $secao = $this->Usuarios->Secoes->get($user['secao_trabalho_id']);
        
        // Se a seção não estiver aberta ainda
        if (empty($secao->abertura))
        {
            $this->Flash->error('É preciso abrir a Seção ' . $secao->id . ' - ' . $secao->nome . ' para poder liberar eleitor!');
            $this->redirect(['controller' => 'Secoes', 'action' => 'abrir']);
        }
        elseif (!empty($secao->fechamento))
        {
            $this->Flash->error('A Seção ' . $secao->id . ' - ' . $secao->nome . ' já está fechada!');   
        }
        else
        {
            if ($this->request->is('post')) {
                if (isset($this->request->data['busca'])) {
                    if (empty($this->request->data['busca'])) 
                        $this->Flash->error('Entre com parte do nome ou com o CPF do eleitor.');
                    else {
                        // limitar busca a no mínimo 03 caracteres
                        if (strlen($this->request->data['busca']) < 3)
                            $this->Flash->error('Entre com pelo menos 03 caracteres para efetuar uma busca mais precisa.');
                        else {
                            // Caso esteja setado "eleitor_id" via POST, o mesário está submetendo uma liberação para votar ou está desfazendo uma liberação
                            if (isset($this->request->data['eleitor_id'])) {
                                // Verifico se existe usuario com esse ID
                                if ($this->Usuarios->exists(['id' => $this->request->data['eleitor_id']])) {
                                    $eleitor = $this->Usuarios->get($this->request->data['eleitor_id']);
                                    
                                    // Se o usuário não for MESÁRIO
                                    if (!in_array($user['grupo_id'], [2])) {
                                        $this->Flash->error('Apenas Mesários podem liberar um eleitor para votação.');   
                                        if (!empty($user))
                                            $this->Usuarios->Auditorias->add(['acao' => 'Erro: usuário não mesário tentou liberar eleitor para votação: ' . $eleitor->nome . ' (' . $eleitor->cpf . ')', 'usuario_id' => $user['id'], 'secao_id' => $user['secao_trabalho_id']]); 
                                    // e se for de SEÇÃO diferente do eleitor 
                                    } elseif ($user['secao_trabalho_id'] != $eleitor->secao_id) {
                                        $this->Flash->error('O Mesário só pode liberar um eleitor pertencente a sua Seção de trabalho.');    
                                        if (!empty($user))
                                            $this->Usuarios->Auditorias->add(['acao' => 'Erro: usuário tentou liberar eleitor de outra seção para votação: ' . $eleitor->nome . ' (' . $eleitor->cpf . ')', 'usuario_id' => $user['id'], 'secao_id' => $user['secao_trabalho_id']]); 
                                    // e a situação do eleitor não for AGUARDANDO LIBERAÇÃO
                                    } elseif ($eleitor->status_id != 1) {
                                        if ($eleitor->status_id == 2)
                                            // Se o mesário realmente quer "Desfazer a liberação" do eleitor
                                            if ($this->request->data['acao'] == 'desfazer_liberacao')
                                            {
                                                $eleitor = $this->Usuarios->patchEntity($eleitor, ['status_id' => 1]);
                                                if ($this->Usuarios->save($eleitor)) {
                                                    $this->Flash->success('Desfeita a liberação para votação.');                                        
                                                    if (!empty($user))
                                                        $this->Usuarios->Auditorias->add(['acao' => 'Desfez a liberação para votação do eleitor: ' . $eleitor->nome . ' (' . $eleitor->cpf . ')', 'usuario_id' => $user['id'], 'secao_id' => $user['secao_trabalho_id']]);
                                                    $this->Usuarios->Auditorias->add(['acao' => 'Desfeita a liberação para votação do eleitor', 'usuario_id' => $eleitor->id, 'secao_id' => $eleitor->secao_id]);
                                                }
                                            }
                                            // Se está apenas tentando liberar novamente um usuário já liberado
                                            else 
                                                $this->Flash->error('O eleitor já está liberado para votar.');    
                                        else
                                            $this->Flash->error('O eleitor deve estar com a situação AGUARDANDO LIBERAÇÃO para poder ser liberado ao voto.');    
                                    }
                                    /**
                                     * Caso todas as proposições anteriores sejam aceitas, verificamos se não existe nenhum outro eleitor liberado para votação nesta seção 
                                     * (ou seja, um eleitor que foi liberado para votação mas não votou)
                                     * e então liberamos o eleitor para o voto
                                     */ 
                                    else {
                                        // Verifico se não tem mais nenhum eleitor LIBERADO PARA VOTAÇÃO nesta Seção
                                        if ($this->Usuarios->possoLiberarEleitorOuFecharSecao($user['secao_trabalho_id'])) 
                                        {
                                            $eleitor = $this->Usuarios->patchEntity($eleitor, ['status_id' => 2]);
                                            if ($this->Usuarios->save($eleitor)) {
                                                $this->Flash->success('Eleitor liberado para votação.');                                        
                                                if (!empty($user))
                                                    $this->Usuarios->Auditorias->add(['acao' => 'Liberou eleitor para votação: ' . $eleitor->nome . ' (' . $eleitor->cpf . ')', 'usuario_id' => $user['id'], 'secao_id' => $user['secao_trabalho_id']]);
                                                $this->Usuarios->Auditorias->add(['acao' => 'Eleitor liberado para votação', 'usuario_id' => $eleitor->id, 'secao_id' => $eleitor->secao_id]);
                                            } 
                                            else {
                                                $this->Flash->error('Não foi possível liberar o eleitor para o voto. Tente novamente. Se o problema persistir entre em contato com o gestor.');
                                                if (!empty($user))
                                                    $this->Usuarios->Auditorias->add(['acao' => 'Erro: ao liberar eleitor para votação: ' . $eleitor->nome . ' (' . $eleitor->cpf . ')', 'usuario_id' => $user['id'], 'secao_id' => $user['secao_trabalho_id']]);
                                            }
                                        }
                                        else {
                                            $this->Flash->error('O último eleitor LIBERADO PARA VOTAÇÃO ainda não votou. Você deve aguardar ele concluir a votação para liberar um novo eleitor ou então DESFAZER a liberação do mesmo.');    
                                        }
                                    }
                                }
                                else
                                    $this->Flash->error('Não existe eleitor com o ID informado. Contacte o gestor.');
                            }

                            // Fazer a busca por parte do NOME ou parte do CPF
                            $eleitor = $this->Usuarios->find('all', [
                                'conditions' => [
                                    'OR' => [
                                        'Usuarios.nome ilike' => '%' . str_replace(' ', '%', $this->request->data['busca']) . '%',
                                        'cpf ilike' => '%' . str_replace(' ', '%', $this->request->data['busca']) . '%'
                                    ]
                                ],
                                'contain' => ['Secoes', 'Status', 'Tipos']
                            ]);

                            $this->set('eleitor', $eleitor->toArray());
                        }
                    }
                }
            }
        }
        $this->set('user', $user);
        $this->set('usuario', $usuario);
        $this->set('_serialize', ['usuario']);
    }

    /*
     *  Função para buscar usuario
     */   
    public function buscar()
    {
        $usuario = $this->Usuarios->newEntity();
        $user = $this->Auth->user();

        if ($this->request->is('post')) {
            if (isset($this->request->data['busca'])) {
                if (empty($this->request->data['busca'])) 
                    $this->Flash->error('Entre com parte do nome ou com o CPF do usuário.');
                else {
                    // limitar busca a no mínimo 03 caracteres
                    if (strlen($this->request->data['busca']) < 3)
                        $this->Flash->error('Entre com pelo menos 03 caracteres para efetuar uma busca mais precisa.');
                    else {
                        // Fazer a busca por parte do NOME ou parte do CPF
                        $eleitor = $this->Usuarios->find('all', [
                            'conditions' => [
                                'OR' => [
                                    'Usuarios.nome ilike' => '%' . str_replace(' ', '%', $this->request->data['busca']) . '%',
                                    'cpf ilike' => '%' . str_replace(' ', '%', $this->request->data['busca']) . '%'
                                ]
                            ],
                            'contain' => ['Secoes', 'Status']
                        ]);

                        $this->set('eleitor', $eleitor->toArray());
                    }
                }
            }
        }
        $this->set('user', $user);
        $this->set('usuario', $usuario);
        $this->set('_serialize', ['usuario']);
    }

    /*
     *  Função para listar mesarios
     */   
    public function listarMesarios()
    {
        $usuario = $this->Usuarios->newEntity();
        $user = $this->Auth->user();

        // Fazer a busca 
        $eleitor = $this->Usuarios->find('all', [
            'conditions' => [
                'OR' => [
                    'Usuarios.grupo_id' => '2'
                ]
            ],
            'contain' => ['Secoes', 'Status'],
            'order' => ['Usuarios.nome']
        ]);

        $this->set('eleitor', $eleitor->toArray());
                    
        $this->set('user', $user);
        $this->set('usuario', $usuario);
        $this->set('_serialize', ['usuario']);
    }

    public function listaPorSecao()
    {
        $user = $this->Auth->user();

        if ($this->request->is('post'))
        {
            if (isset($this->request->data['secao_id']))
            {
                if (!empty($this->request->data['secao_id']))
                {
                    if (in_array($user['grupo_id'], [3, 4])) // Se for ADMINISTRADOR ou GESTOR, pode listar todas as Seções
                    {
                        if ($this->request->data['secao_id'] == 'todas')
                        {
                            set_time_limit(0);
                            ini_set('memory_limit', '-1');
                            $conditions = [];
                        }
                        else
                            $conditions = ['Secoes.id' => $this->request->data['secao_id']];
                    }
                    elseif (in_array($user['grupo_id'], [2])) // Se for Mesário pode listar apenas sua Seção de Trabalho
                    {
                        if ($this->request->data['secao_id'] != $user['secao_trabalho_id'])
                            $this->Flash->error('O mesário só pode listar os eleitores de sua Seção de Trabalho.');
                                    
                        $conditions = ['Secoes.id' => $user['secao_trabalho_id']];
                    }

                    $eleitores = $this->Usuarios->find('all', [
                        'contain' => ['Secoes', 'Tipos', 'Status'],
                        'conditions' => $conditions,
                        'order' => ['Usuarios.nome']
                    ]);

                    $status_eleitores = new Collection($eleitores);
                    
                    $contagem = $status_eleitores->countBy(function($eleitor){
                        return $eleitor->status_id;
                    })->toArray(); 

                    $this->set(compact('eleitores', 'contagem'));
                }
                else
                    $this->Flash->default('Selecione uma Seção.');
            }
        }

        $secoes = $this->Usuarios->Secoes->find('list', ['order' => ['Secoes.nome']]);
        $status = $this->Usuarios->Status->find('list');
        $this->set(compact('secoes', 'status'));
    }

    public function listaPorCampus()
    {
        $user = $this->Auth->user();

        if ($this->request->is('post'))
        {
            if (isset($this->request->data['campus_id']))
            {
                if (!empty($this->request->data['campus_id']))
                {
                    if ($this->request->data['campus_id'] == 'todos')
                        $conditions = [];
                    else
                        $conditions = ['Campi.id' => $this->request->data['campus_id']];

                    $eleitores = $this->Usuarios->find('all', [
                        'contain' => ['Secoes' => ['Campi'], 'Tipos', 'Status'],
                        'conditions' => $conditions,
                        'order' => ['Campi.nome', 'Secoes.nome', 'Usuarios.nome']
                    ]);

                    $this->set(compact('eleitores'));
                }
                else
                    $this->Flash->default('Selecione um Campus.');
            }
        }

        $campi = $this->Usuarios->Secoes->Campi->find('list', ['order' => ['Campi.nome']]);
        $this->set(compact('campi'));
    }
}
