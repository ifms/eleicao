<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Campi Controller
 *
 * @property \App\Model\Table\CampiTable $Campi
 */
class CampiController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('campi', $this->paginate($this->Campi));
        $this->set('_serialize', ['campi']);
    }

    /**
     * View method
     *
     * @param string|null $id Campus id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $campus = $this->Campi->get($id, [
            'contain' => ['Candidatos' => ['Tipos']]
        ]);
        $this->set('campus', $campus);
        $this->set('_serialize', ['campus']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $campus = $this->Campi->newEntity();
        if ($this->request->is('post')) {
            $campus = $this->Campi->patchEntity($campus, $this->request->data);
            if ($this->Campi->save($campus)) {
                $this->Flash->success(__('The campus has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The campus could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('campus'));
        $this->set('_serialize', ['campus']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Campus id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $campus = $this->Campi->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $campus = $this->Campi->patchEntity($campus, $this->request->data);
            if ($this->Campi->save($campus)) {
                $this->Flash->success(__('The campus has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The campus could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('campus'));
        $this->set('_serialize', ['campus']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Campus id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $campus = $this->Campi->get($id);
        if ($this->Campi->delete($campus)) {
            $this->Flash->success(__('The campus has been deleted.'));
        } else {
            $this->Flash->error(__('The campus could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
