<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use App\Model\Entity\Configuracao;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'authorize' => ['Controller'],
            'loginRedirect' => [
                'controller' => 'Pages',
                'action' => 'paginaInterna'
            ],
            'logoutRedirect' => [
                'controller' => 'Votos',
                'action' => 'votarAutenticado'
            ],
            'authenticate' => [
                'Form' => [
                    'userModel' => 'Usuarios',
                    'fields' => [
                        'username' => 'cpf',
                        'password' => 'senha'
                    ]
                ],
                'Ad' // Autenticação via Active Directory
            ],
            'loginAction' => [
                'controller' => 'Usuarios',
                'action' => 'login'
            ],
            'authError' => 'Apenas usuários autenticados podem acessar este endereço, entre usando suas credenciais.'
        ]);


    }

    public function isAuthorized($user)
    {
        // Se for Administrador
        if ($user['grupo_id'] == 4) {
            return true;
        }
        else {
            $permissoes = $this->loadModel('Permissoes');
            $permissoes = $this->Permissoes->find('all', [
                'conditions' => [
                    'grupo_id' => $user['grupo_id']
                ]
            ]);

            foreach ($permissoes as $permissao) {
                if ($this->request->controller == $permissao['controller'])
                {
                    $acoes_permitidas = explode(',', $permissao['action']);
                    
                    foreach ($acoes_permitidas as $id => $acao) {
                        $acoes_permitidas[$id] = trim($acao);
                    }

                    if (in_array($this->request->action, $acoes_permitidas) OR in_array('*', $acoes_permitidas))
                        return true;
                }
            }
        }

        // Default deny
        $this->Flash->error('Você não possui permissão para executar essa operação.');
        return false;
    }

    public function beforeFilter(Event $event)
    {
        //$this->Auth->allow(['index', 'view', 'display', 'login', 'add', 'delete', 'edit']);
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }

    public function configuracao($chave = null) {
        $configuracoes = new Configuracao();
        return $configuracoes->le($chave);
    }
}
