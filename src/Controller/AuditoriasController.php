<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Auditorias Controller
 *
 * @property \App\Model\Table\AuditoriasTable $Auditorias
 */
class AuditoriasController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Usuarios' => ['Grupos'], 'Secoes'],
            'order' => ['Auditorias.id DESC']
        ];
        $this->set('auditorias', $this->paginate($this->Auditorias));
        $this->set('_serialize', ['auditorias']);
    }

    /**
     * View method
     *
     * @param string|null $id Auditoria id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $auditoria = $this->Auditorias->get($id, [
            'contain' => ['Usuarios', 'Secoes']
        ]);
        $this->set('auditoria', $auditoria);
        $this->set('_serialize', ['auditoria']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $auditoria = $this->Auditorias->newEntity();
        if ($this->request->is('post')) {
            $auditoria = $this->Auditorias->patchEntity($auditoria, $this->request->data);
            if ($this->Auditorias->save($auditoria)) {
                $this->Flash->success(__('The auditoria has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The auditoria could not be saved. Please, try again.'));
            }
        }
        $usuarios = $this->Auditorias->Usuarios->find('list', ['limit' => 200]);
        $secoes = $this->Auditorias->Secoes->find('list', ['limit' => 200]);
        $this->set(compact('auditoria', 'usuarios', 'secoes'));
        $this->set('_serialize', ['auditoria']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Auditoria id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $auditoria = $this->Auditorias->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $auditoria = $this->Auditorias->patchEntity($auditoria, $this->request->data);
            if ($this->Auditorias->save($auditoria)) {
                $this->Flash->success(__('The auditoria has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The auditoria could not be saved. Please, try again.'));
            }
        }
        $usuarios = $this->Auditorias->Usuarios->find('list', ['limit' => 200]);
        $secoes = $this->Auditorias->Secoes->find('list', ['limit' => 200]);
        $this->set(compact('auditoria', 'usuarios', 'secoes'));
        $this->set('_serialize', ['auditoria']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Auditoria id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $auditoria = $this->Auditorias->get($id);
        if ($this->Auditorias->delete($auditoria)) {
            $this->Flash->success(__('The auditoria has been deleted.'));
        } else {
            $this->Flash->error(__('The auditoria could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
