<?php
namespace App\Controller;

use Cake\Event\Event;
use App\Controller\AppController;
use Cake\Collection\Collection;
use Cake\Mailer\Email;

/**
 * Secoes Controller
 *
 * @property \App\Model\Table\SecoesTable $Secoes
 */
class SecoesController extends AppController
{

    /*
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // Allow users login and logout.
        $this->Auth->allow(['index', 'add']);
    }
    */

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Campi'],
            'order' => ['Secoes.id']
        ];
        $this->set('secoes', $this->paginate($this->Secoes));
        $this->set('_serialize', ['secoes']);
    }

    /**
     * View method
     *
     * @param string|null $id Secao id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $secao = $this->Secoes->get($id, [
            'contain' => ['Campi']
        ]);
        $this->set('secao', $secao);
        $this->set('_serialize', ['secao']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $secao = $this->Secoes->newEntity();
        if ($this->request->is('post')) {
            $secao = $this->Secoes->patchEntity($secao, $this->request->data);
            if ($this->Secoes->save($secao)) {
                $this->Flash->success(__('The secao has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The secao could not be saved. Please, try again.'));
            }
        }
        $campi = $this->Secoes->Campi->find('list');
        $this->set(compact(['secao', 'campi']));
        $this->set('_serialize', ['secao']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Secao id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $secao = $this->Secoes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $secao = $this->Secoes->patchEntity($secao, $this->request->data);
            if ($this->Secoes->save($secao)) {
                $this->Flash->success(__('The secao has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The secao could not be saved. Please, try again.'));
            }
        }
        $campi = $this->Secoes->Campi->find('list');
        $this->set(compact(['secao', 'campi']));
        $this->set('_serialize', ['secao']);

        //debug($secao);
    }

    /**
     * Delete method
     *
     * @param string|null $id Secao id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $secao = $this->Secoes->get($id);
        if ($this->Secoes->delete($secao)) {
            $this->Flash->success(__('The secao has been deleted.'));
        } else {
            $this->Flash->error(__('The secao could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function abrir()
    {
        $user = $this->Auth->user();

        $secao = $this->Secoes->get($user['secao_trabalho_id']);

        // Se a seção não estiver aberta ainda e nem estiver fechada
        if (!empty($secao->abertura))
        {
            $this->Flash->error('A Seção ' . $secao->id . ' - ' . $secao->nome . ' já foi aberta!');
        }
        elseif (!empty($secao->fechamento))
        {
            $this->Flash->error('A Seção ' . $secao->id . ' - ' . $secao->nome . ' já está fechada e não pode ser aberta novamente!');   
        }
        else
        {
            $agora = date('Y-m-d H:i:s');
            // Se já deu a hora permitida para abertura das Seções e ainda não passou do fechamento
            if(($agora > $secao->abertura_autorizada->format('Y-m-d H:i:s')) and ($agora < $secao->fechamento_autorizado->format('Y-m-d H:i:s')))
            {
                // Listar quantidade de votos por candidatos desta seção para Zerésima
                $votos = $this->Secoes->Votos->find('all', [
                    'conditions' => ['Votos.secao_id' => $secao->id]
                ]);

                // Contagem por CANDIDATO, inclusive os nulos na chave "null" ou '' (vazio)
                $todos_votos = new Collection($votos);
                $votos_por_candidato = $todos_votos->countBy('candidato_id')->toArray();

                // Contar votos brancos e nulos
                $votos_brancos_nulos = $todos_votos->countBy(function ($voto) {
                    if (!$voto->candidato_id) {
                        return $voto->branco == true ? 'branco' : 'nulo';
                    }
                })->toArray();

                // Lista de candidatos
                $candidatos = $this->Secoes->Votos->Candidatos->find('all', [
                    'contain' => ['Tipos', 'Campi'],
                    'order' => ['Tipos.nome', 'Campi.nome', 'Candidatos.nome']
                ]);

                $this->set('total_votos', $votos->count());
                $this->set(compact(['secao','votos_por_candidato', 'candidatos', 'votos_brancos_nulos', 'user']));
                $this->set('_serialize', ['secao']);

                // Clicou para abrir a seção
                if (isset($this->request->data['id']))
                {
                    if (isset($this->request->data['declaracao']))
                    {
                        $email_comissao = parent::configuracao('email_comissao');

                        // Enviar email
                        $email = new Email('default');
                        $email->to($email_comissao)
                            ->emailFormat('html')
                            ->subject('Aberta a Seção ' . $secao->id . ' - ' . $secao->nome . ' por ' . $user['nome']);

                        if ($email->send($this->request->data['declaracao']))
                        {
                            // Editar a abertura da Seção
                            $secao->abertura = date('Y-m-d H:i:s');
                            if($this->Secoes->save($secao))
                            {
                                $this->Secoes->Auditorias->add(['acao' => 'Realizada abertura da Seção. Relatório encaminhado ao email: ' . $email_comissao . '.', 'usuario_id' => $user['id'], 'secao_id' => $secao->id]); 
                                $this->Flash->success('Seção aberta com sucesso.');
                                $this->redirect('/');
                            }
                            else
                            {
                                $this->Secoes->Auditorias->add(['acao' => 'Erro ao realizar abertura da Seção. Falha ao gravar data de abertura', 'usuario_id' => $user['id'], 'secao_id' => $secao->id]); 
                                $this->Flash->success('Erro ao abrir seção. Tente novamente.');
                            }
                        }
                        else
                            $this->Secoes->Auditorias->add(['acao' => 'Erro ao tentar abrir Seção: Erro ao encaminhar o relatório de abertura.', 'usuario_id' => $user['id'], 'secao_id' => $secao->id]); 
                    }
                }
                else
                    $this->Flash->success('Para concluir a abertura, confira os dados e clique no botão "Abrir" no final da página.');
            }
            else
            {
                $this->Flash->error('O horário não permite a abertura da Seção.');
            }
        }
    }

    public function fechar()
    {
        $user = $this->Auth->user();

        $secao = $this->Secoes->get($user['secao_trabalho_id']);

        // Se a seção já estiver aberta e ainda não estiver fechada
        if (empty($secao->abertura))
        {
            $this->Flash->error('A Seção ' . $secao->id . ' - ' . $secao->nome . ' ainda não foi aberta!');
        }
        elseif (!empty($secao->fechamento))
        {
            $this->Flash->error('A Seção ' . $secao->id . ' - ' . $secao->nome . ' já está fechada!');
        }
        else
        {
            // Se a seção for AUTENTICADA, ou seja, o voto for via autenticação, então todos os usuários estarão LIBERADOS PARA VOTAÇÃO
            // Caso contrário, verifico se não ficou nenhum eleitor com status LIBERADO PARA VOTAÇÃO (sem ter votado)
            if ($secao->tipo == 'autenticada' OR $this->Secoes->Usuarios->possoLiberarEleitorOuFecharSecao($user['secao_trabalho_id']))
            {
                $agora = date('Y-m-d H:i:s');
                // Se já deu a hora permitida para fechamento das Seções
                if(($agora > $secao->abertura_autorizada->format('Y-m-d H:i:s')) and ($agora >= $secao->fechamento_autorizado->format('Y-m-d H:i:s')))
                {
                    // Listar quantidade de votos por candidatos desta seção para relatório final
                    $votos = $this->Secoes->Votos->find('all', [
                        'conditions' => ['Votos.secao_id' => $secao->id]
                    ]);

                    // Contagem por CANDIDATO, inclusive os nulos na chave "null" ou '' (vazio)
                    $todos_votos = new Collection($votos);
                    $votos_por_candidato = $todos_votos->countBy('candidato_id')->toArray();

                    // Contar votos brancos e nulos
                    $votos_brancos_nulos = $todos_votos->countBy(function ($voto) {
                        if (!$voto->candidato_id) {
                            return $voto->branco == true ? 'branco' : 'nulo';
                        }
                    })->toArray();

                    // Lista de candidatos
                    $candidatos = $this->Secoes->Votos->Candidatos->find('all', [
                        'contain' => ['Tipos', 'Campi'],
                        'order' => ['Tipos.nome', 'Campi.nome', 'Candidatos.nome']
                    ]);

                    $this->set('total_votos', $votos->count());
                    $this->set(compact(['secao','votos_por_candidato', 'candidatos', 'votos_brancos_nulos', 'user']));
                    $this->set('_serialize', ['secao']);

                    // Quantitativo de eleitores por Categoria
                    $eleitores = $this->Secoes->Usuarios->find('all', ['conditions' => ['Usuarios.secao_id' => $secao->id]]);

                    $eleitores = new Collection($eleitores);
                    $eleitores = $eleitores->groupBy('status_id');
                    $qt_eleitores = array();

                    foreach ($eleitores as $status_id => $eleitores) {
                        $collection = new Collection($eleitores);
                        $qt_eleitores[$status_id] = $collection->countBy(function($eleitor) {
                            return $eleitor->tipo_id;
                        })->toArray();    
                    }

                    $status = $this->Secoes->Usuarios->Status->find('list');

                    foreach ($status as $status_id => $s) {
                        if(!isset($qt_eleitores[$status_id]))
                            $qt_eleitores[$status_id] = array();
                    }

                    $tipos = $this->Secoes->Usuarios->Tipos->find('list');
                    $this->set(compact(['qt_eleitores', 'tipos', 'status']));

                    // Clicou para fechar a seção
                    if (isset($this->request->data['id']))
                    {
                        if (isset($this->request->data['declaracao']))
                        {
                            $email_comissao = parent::configuracao('email_comissao');

                            // Enviar email
                            $email = new Email('default');
                            $email->to($email_comissao)
                                ->emailFormat('html')
                                ->subject('Fechada a Seção ' . $secao->id . ' - ' . $secao->nome . ' por ' . $user['nome']);

                            if ($email->send($this->request->data['declaracao']))
                            {
                                // Editar o fechamento da Seção
                                $secao->fechamento = date('Y-m-d H:i:s');
                                if($this->Secoes->save($secao))
                                {
                                    $this->Secoes->Auditorias->add(['acao' => 'Realizado o fechamento da Seção. Relatório encaminhado ao email: ' . $email_comissao . '.', 'usuario_id' => $user['id'], 'secao_id' => $secao->id]); 
                                    $this->Flash->success('Seção fechada com sucesso.');
                                }
                                else
                                {
                                    $this->Secoes->Auditorias->add(['acao' => 'Erro ao realizar fechamento da Seção. Falha ao gravar data de fechamento', 'usuario_id' => $user['id'], 'secao_id' => $secao->id]); 
                                    $this->Flash->success('Erro ao fechar seção. Tente novamente.');
                                }
                            }
                            else
                                $this->Secoes->Auditorias->add(['acao' => 'Erro ao tentar fechar Seção: Erro ao encaminhar o relatório de fechamento.', 'usuario_id' => $user['id'], 'secao_id' => $secao->id]); 
                        }
                    }
                    else
                        $this->Flash->success('Para concluir o fechamento, confira os dados e clique no botão "Fechar" no final da página.');
                }
                else
                {
                    $this->Flash->error('O horário não permite o fechamento da Seção. O horário autorizado para o fechamento é apartir de: ' . $secao->fechamento_autorizado);
                }
            }
            else
                $this->Flash->error('O último eleitor LIBERADO PARA VOTAÇÃO ainda não votou. Você deve aguardar ele concluir a votação para fechar a Seção ou então DESFAZER a liberação do mesmo.'); 
        }
    }
}
