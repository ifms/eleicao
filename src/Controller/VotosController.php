<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Collection\Collection;
/**
 * Votos Controller
 *
 * @property \App\Model\Table\VotosTable $Votos
 */
class VotosController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // Allow users login and logout.
        $this->Auth->allow(['votar', 'votarAutenticado', 'consultarProtocolo', 'testeSom']);
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Candidatos', 'Secoes']
        ];
        $this->set('votos', $this->paginate($this->Votos));
        $this->set('_serialize', ['votos']);
    }

    /**
     * View method
     *
     * @param string|null $id Voto id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $voto = $this->Votos->get($id, [
            'contain' => ['Candidatos', 'Secoes']
        ]);
        $this->set('voto', $voto);
        $this->set('_serialize', ['voto']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $voto = $this->Votos->newEntity();
        if ($this->request->is('post')) {
            $voto = $this->Votos->patchEntity($voto, $this->request->data);
            if ($this->Votos->save($voto)) {
                $this->Flash->success(__('The voto has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The voto could not be saved. Please, try again.'));
            }
        }
        $candidatos = $this->Votos->Candidatos->find('list', ['limit' => 200]);
        $secoes = $this->Votos->Secoes->find('list', ['limit' => 200]);
        $this->set(compact('voto', 'candidatos', 'secoes'));
        $this->set('_serialize', ['voto']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Voto id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $voto = $this->Votos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $voto = $this->Votos->patchEntity($voto, $this->request->data);
            if ($this->Votos->save($voto)) {
                $this->Flash->success(__('The voto has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The voto could not be saved. Please, try again.'));
            }
        }
        $candidatos = $this->Votos->Candidatos->find('list', ['limit' => 200]);
        $secoes = $this->Votos->Secoes->find('list', ['limit' => 200]);
        $this->set(compact('voto', 'candidatos', 'secoes'));
        $this->set('_serialize', ['voto']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Voto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $voto = $this->Votos->get($id);
        if ($this->Votos->delete($voto)) {
            $this->Flash->success(__('The voto has been deleted.'));
        } else {
            $this->Flash->error(__('The voto could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function votar()
    {
        // Se a votação for AUTENTICADA, redirecionamos para a tela de votação autenticada
        if (parent::configuracao('votacao_autenticada') == 'true')
            $this->redirect(['action' => 'votarAutenticado']);

        // Alterar para layout do Eleitor
        $this->viewBuilder()->layout('eleitor');
        $quantidade_candidatos_eleitor_pode_votar = parent::configuracao('quantidade_candidatos_eleitor_pode_votar');
        $permitir_nulo = parent::configuracao('permitir_nulo');
        $this->set(compact('quantidade_candidatos_eleitor_pode_votar', 'permitir_nulo'));

        // Verificar período de votação
        $agora = date('Y-m-d H:i:s');
        if($agora < parent::configuracao('inicio_votacao'))
        {
            $this->Flash->error('O período de votação ainda não começou.');
        } 
        elseif ($agora > parent::configuracao('termino_votacao'))
        {
            $this->Flash->error('O período de votação está encerrado.');
        }
        else
        {
            if ($this->request->is('post')) 
            {
                if (isset($this->request->data['cpf']))
                {
                    if (!empty($this->request->data['cpf'])) 
                    {
                        $tabela_usuarios = TableRegistry::get('Usuarios');

                        // Verifico se o CPF informado é valido
                        if ($tabela_usuarios->validarCpf($this->request->data['cpf'])) 
                        {
                            $this->loadModel('Usuarios');

                            // Verifico se existe algum eleitor com esse CPF
                            if ($this->Usuarios->exists(['cpf' => $this->request->data['cpf']])) 
                            {
                                $eleitor = $this->Usuarios->find('all', [
                                    'conditions' => [
                                        'cpf' => $this->request->data['cpf'],
                                        'status_id' => 2 // pega somente registro com status_id = 2 (resolve quando o CPF eh duplicado)
                                    ],
                                    'contain' => ['Tipos', 'Secoes']
                                ]);

                                if( $eleitor->isEmpty() ) // se nao ha eleitor liberado para votar
                                {
                                    $this->Flash->error('Eleitor não está liberado para votação. Verifique junto ao responsável pela sua Seção eleitoral.');
                                    $eleitor = $this->Usuarios->find('all', [
                                        'conditions' => [
                                            'cpf' => $this->request->data['cpf']
                                        ]
                                    ])->toArray();
                                    $this->Usuarios->Auditorias->add(['acao' => 'Aviso: eleitor não liberado para votação tentou votar.', 'usuario_id' => $eleitor[0]->id, 'secao_id' => $eleitor[0]->secao_id]); 
                                    return $this->redirect(['action' => 'votar']);
                                }

                                // Verificar se o eleitor está LIBERADO PARA VOTAÇÃO
                                foreach ($eleitor as $e) {

                                    if ($e->status_id != 2)    
                                    {
                                        if ($e->status_id == 3) {
                                            $this->Flash->error('Eleitor já votou.');
                                            $this->Usuarios->Auditorias->add(['acao' => 'Aviso: eleitor que já votou tentou votar novamente.', 'usuario_id' => $e->id, 'secao_id' => $e->secao_id]); 
                                        }
                                        else {
                                            $this->Flash->error('Eleitor não está liberado para votação. Verifique junto ao responsável pela sua Seção eleitoral.');
                                            $this->Usuarios->Auditorias->add(['acao' => 'Aviso: eleitor não liberado para votação tentou votar.', 'usuario_id' => $e->id, 'secao_id' => $e->secao_id]); 
                                        }
                                        return $this->redirect(['action' => 'votar']);
                                    }

                                    $tipo_id = $e->tipo_id; // Guardamos o tipo_id para consulta de candidatos
                                    $secao_id = $e->secao_id; // Guardamos a secao_id para gravar nos votos
                                    $eleitor_id = $e->id; // Guardamos o id do eleitor para alterar situação após registro dos votos
                                    $campus_id = $e->secao->campus_id; // Guardamos o campus para selecionar apenas candidatos do campus dele caso "permitir_votar_apenas_candidatos_do_mesmo_campus = true"

                                    if (!isset($this->request->data['confirmou_dados']))
                                    {
                                        // Registro de auditoria
                                        $this->Usuarios->Auditorias->add(['acao' => 'Eleitor liberado para votação entrou com seu CPF para votação.', 'usuario_id' => $e->id, 'secao_id' => $e->secao_id]); 
                                    }
                                }

                                // 1ª etapa - Confirmação do eleitor
                                // Repasamos as informações coletadas sobre o eleitor para que ele confirme:
                                $this->set(compact('eleitor'));

                                // 2ª etapa - eleitor clicou no botão "Sim, prosseguir"
                                if (isset($this->request->data['confirmou_dados']) AND $this->request->data['confirmou_dados'])
                                {
                                    if (!isset($this->request->data['Candidatos'])) {
                                        // Registro de auditoria
                                        $this->Usuarios->Auditorias->add(['acao' => 'Eleitor confirmou seus dados para a votação.', 'usuario_id' => $e->id, 'secao_id' => $e->secao_id]); 
                                    }

                                    /*** Carregar CANDIDATOS ***/
                                    $conditions = [];

                                    // Se a configuração do sistema permitir apenas o voto em candidatos do mesmo tipo
                                    if (parent::configuracao('permitir_votar_apenas_candidatos_do_mesmo_tipo') == 'true')
                                        $conditions['Candidatos.tipo_id'] = $tipo_id;

                                    // Se a configuração do sistema permitir apenas o voto em candidatos do mesmo campus
                                    if (parent::configuracao('permitir_votar_apenas_candidatos_do_mesmo_campus') == 'true')
                                        $conditions['Candidatos.campus_id'] = $campus_id;

                                    // Carregar candidatos conforme o TIPO do eleitor (estudante, téc. Administrativo ou docente)
                                    $candidatos = $this->Votos->Candidatos->find('all', [
                                        'conditions' => $conditions,
                                        'contain' => ['Campi', 'Tipos'],
                                        'order' => ['Candidatos.nome']
                                    ]);

                                    // Se tem candidatos do mesmo TIPO do eleitor (estudante, docente, tec. administrativo)
                                    if($candidatos->count() > 0) {
                                        $this->set(compact('candidatos'));
                                        $lista_candidatos_votados_validos = array();

                                        // Se encaminhou votação
                                        if (isset($this->request->data['Candidatos'])) {
                                            foreach ($this->request->data['Candidatos'] as $candidato) {
                                                foreach ($candidatos as $candidato_valido) {
                                                    // Se o ID do candidato selecionado pertence a lista de candidatos aptos para o eleitor, guardamos
                                                    if ($candidato['id'] != 0 AND $candidato['id'] == $candidato_valido->id)
                                                        $lista_candidatos_votados_validos[] = $candidato['id']; 
                                                }
                                            }

                                            // Se selecionou até a quantidade máxima de candidatos que o eleitor pode votar
                                            if (count($lista_candidatos_votados_validos) <= $quantidade_candidatos_eleitor_pode_votar) 
                                            {
                                                $erro_ao_registrar_voto = false;
                                                $votos_registrados = array();

                                                // Reservei um código de votação
                                                $codigo = $this->Votos->reserva_codigo();

                                                // Se consegui reservar um código de votação
                                                if (!empty($codigo) AND $codigo['livre'])
                                                {
                                                    // Anulou o VOTO
                                                    if (isset($this->request->data['botao_anular']) AND $this->request->data['botao_anular'] == 1) 
                                                    {
                                                        // Verifico se é está configurado para permitir votos nulos
                                                        if ($permitir_nulo == 'true')
                                                        {
                                                            $voto = $this->Votos->newEntity();
                                                            $voto->candidato_id = null;
                                                            $voto->secao_id = $secao_id;
                                                            $voto->registro = date('Y-m-d H:i:s');
                                                            $voto->branco = null;
                                                            $voto->nulo = true;
                                                            $voto->codigo = $codigo['codigo'] . '-' . $codigo['dv'];
                                                            if (!$this->Votos->save($voto))
                                                                $erro_ao_registrar_voto = true;
                                                            else
                                                                $votos_registrados[] = $voto->id;
                                                        }
                                                        else {
                                                            $erro_ao_registrar_voto = true;
                                                            $this->Flash->error('Essa votação não permite voto nulo. Escolha um candidato.');        
                                                        }
                                                    }
                                                    // Votou em BRANCO
                                                    elseif (count($lista_candidatos_votados_validos) == 0) {
                                                        // Verifico se é está configurado para permitir votos brancos
                                                        if (parent::configuracao('permitir_branco') == 'true')
                                                        {
                                                            $voto = $this->Votos->newEntity();
                                                            $voto->candidato_id = null;
                                                            $voto->secao_id = $secao_id;
                                                            $voto->registro = date('Y-m-d H:i:s');
                                                            $voto->branco = true;
                                                            $voto->nulo = null;
                                                            $voto->codigo = $codigo['codigo'] . '-' . $codigo['dv'];
                                                            if (!$this->Votos->save($voto))
                                                                $erro_ao_registrar_voto = true;
                                                            else
                                                                $votos_registrados[] = $voto->id;
                                                        }
                                                        else {
                                                            $erro_ao_registrar_voto = true;
                                                            $this->Flash->error('Essa votação não permite voto em branco. Escolha um candidato.');        
                                                        }
                                                    }
                                                    else {
                                                        foreach ($lista_candidatos_votados_validos as $candidato_id) {
                                                            $voto = $this->Votos->newEntity();
                                                            $voto->candidato_id = $candidato_id;
                                                            $voto->secao_id = $secao_id;
                                                            $voto->registro = date('Y-m-d H:i:s');
                                                            $voto->branco = null;
                                                            $voto->nulo = null;
                                                            $voto->codigo = $codigo['codigo'] . '-' . $codigo['dv'];
                                                            if (!$this->Votos->save($voto))
                                                                $erro_ao_registrar_voto = true;
                                                            else
                                                                $votos_registrados[] = $voto->id;
                                                        }
                                                    }

                                                    // Se não houver erro ao registrar o voto, então alteramos a situação do eleitor
                                                    if (!$erro_ao_registrar_voto)
                                                    {
                                                        $usuario = $this->Usuarios->get($eleitor_id);
                                                        $usuario->status_id = 3; // Altero a situação do eleitor para JÁ VOTOU
                                                        if (!$this->Usuarios->save($usuario))
                                                        {
                                                            // Apago os votos registrados
                                                            foreach ($votos_registrados as $voto_registrado) {
                                                                $voto_para_apagar = $this->Votos->get($voto_registrado);
                                                                $this->Votos->delete($voto_para_apagar);
                                                            }
                                                            $this->Flash->error('Ocorreu um erro ao registrar os votos. Por gentileza procure o responsável pela Seção e informe o problema.');        
                                                            $this->Usuarios->Auditorias->add(['acao' => 'Erro: ao registrar os votos do eleitor.', 'usuario_id' => $e->id, 'secao_id' => $e->secao_id]); 
                                                            return $this->redirect(['action' => 'votar']);
                                                        } else {
                                                            $this->Flash->success('<h3>Voto registrado com sucesso!</h3><p>Anote o seu número de protocolo para conferência:<br/><span style="color: red; font-size: 32px; font-weight: bold;">' . $codigo['codigo'] . '-' . $codigo['dv'] . '</span></p><p>Com esse protocolo será possível consultar o seu voto sem a identificação do eleitor, por isso guarde-o em segredo.</p><p>Obrigado!</p><p><button class="success" id="btn_fechar">Clique para encerrar</button></p>');        
                                                            $this->Usuarios->Auditorias->add(['acao' => 'Eleitor votou com sucesso.', 'usuario_id' => $e->id, 'secao_id' => $e->secao_id]); 
                                                            return $this->redirect(['action' => 'votar']);
                                                        }
                                                    } 
                                                    else
                                                    {
                                                        // Apago os votos registrados
                                                        foreach ($votos_registrados as $voto_registrado) {
                                                            $voto_para_apagar = $this->Votos->get($voto_registrado);
                                                            $this->Votos->delete($voto_para_apagar);
                                                        }
                                                        // Registro de auditoria
                                                        $this->Usuarios->Auditorias->add(['acao' => 'Erro: ao registrar votos.', 'usuario_id' => $e->id, 'secao_id' => $e->secao_id]); 
                                                    }
                                                } else {
                                                    $this->Flash->error('Erro ao reservar código de votação. Por gentileza, comunique a mesa da Seção Eleitoral.');        
                                                }
                                            } else
                                                $this->Flash->error('Só é permitida a votação em até ' . $quantidade_candidatos_eleitor_pode_votar . ' candidato(s).');        
                                        }
                                    } else
                                        $this->Flash->error('Não existem candidatos nesta categoria.');        
                                }
                            } 
                            else
                                $this->Flash->error('Seu CPF não está cadastrado no sistema! Verifique se está digitando corretamente.');
                        } 
                        else
                            $this->Flash->error('Número de CPF inválido! Verifique se está digitando corretamente.');
                    } 
                    else
                        $this->Flash->error('Por favor informe seu número de CPF (somente números).');
                }
            }
        }
    }

    /*
     * Cópia da função votar(), porém para um eleitor que está fazendo a votação pela Internet 
     */
    public function votarAutenticado()
    {
        // Alterar para layout do Eleitor
        $this->viewBuilder()->layout('eleitor');
        $quantidade_candidatos_eleitor_pode_votar = parent::configuracao('quantidade_candidatos_eleitor_pode_votar');
        $permitir_nulo = parent::configuracao('permitir_nulo');
        $descricao_eleicao = parent::configuracao('descricao_eleicao');
        $url_edital = parent::configuracao('url_edital');
        $this->set(compact('quantidade_candidatos_eleitor_pode_votar', 'permitir_nulo', 'url_edital', 'descricao_eleicao'));

        // Verificar período de votação
        $agora = date('Y-m-d H:i:s');
        if($agora < parent::configuracao('inicio_votacao'))
        {
            $this->Flash->error('O período de votação ainda não começou.');
        } 
        elseif ($agora > parent::configuracao('termino_votacao'))
        {
            $this->Flash->error('O período de votação está encerrado.');
        }
        else
        {
            if ($this->request->is('post')) 
            {
                $user = $this->Auth->user();

                // Verifico se usuário já está logado
                if (empty($user))
                {
                    $user = $this->Auth->identify();

                    if ($user AND $user != -1) {
                        $this->Auth->setUser($user);

                        // Registrar login
                        $this->loadModel('Usuarios');
                        $this->Usuarios->Auditorias->add(['acao' => 'Login na tela de votação', 'usuario_id' => $user['id'], 'secao_id' => $user['secao_id']]);
                    }
                    elseif ($user != -1)
                    {
                        sleep(2); // Aguardamos 2 segundos - Prevenção de ataques de força bruta
                        $this->Flash->error('SIAPE ou Senha errado(s). Tente novamente.');
                    }
                }

                // Se o usuário estiver autenticado
                if (isset($user) AND !empty($user))
                {
                    if (!empty($user['cpf'])) 
                    {
                        $this->loadModel('Usuarios');

                        // Verifico se existe algum eleitor com esse CPF
                        if ($this->Usuarios->exists(['cpf' => $user['cpf']])) 
                        {
                            $eleitor = $this->Usuarios->find('all', [
                                'conditions' => [
                                    'cpf' => $user['cpf']
                                ],
                                'contain' => ['Tipos', 'Secoes']
                            ]);

                            //debug($eleitor->toArray());
                            // Condição adicionada excepcionalmente por conta de edital de COXIM
                            if (isset($eleitor->toArray()[0]->tipo_id) AND $eleitor->toArray()[0]->tipo_id == 2)
                            {
                                $quantidade_candidatos_eleitor_pode_votar = 5;
                                $this->set(compact('quantidade_candidatos_eleitor_pode_votar'));
                            }

                            $pode_votar = true;

                            // se o tipo de seção for AUTENTICADA, significa que a votação será através de uso de SIAPE e SENHA, portanto não exige a abertura formal da seção. Consideramos então as datas de: abertura autorizada e fechamento autorizado
                            if ($eleitor->toArray()[0]->secao->tipo == 'autenticada')
                            {
                                // Verificamos se a seção eleitoral está aberta para votação
                                if (is_null($eleitor->toArray()[0]->secao->abertura_autorizada) OR strtotime($agora) < strtotime($eleitor->toArray()[0]->secao->abertura_autorizada->i18nFormat('YYYY-MM-dd HH:mm:ss')))
                                {
                                    $this->Flash->error('A Seção eleitoral ainda não está aberta.');
                                    $pode_votar = false;
                                } 
                                elseif(!is_null($eleitor->toArray()[0]->secao->fechamento_autorizado) AND strtotime($agora) > strtotime($eleitor->toArray()[0]->secao->fechamento_autorizado->i18nFormat('YYYY-MM-dd HH:mm:ss')))
                                {
                                    $this->Flash->error('A Seção eleitoral já está fechada.');
                                    $pode_votar = false;
                                }
                            }
                            else
                            {
                                // Verificamos se a seção eleitoral está aberta para votação
                                if(is_null($eleitor->toArray()[0]->secao->abertura) OR $agora < $eleitor->toArray()[0]->secao->abertura)
                                {
                                    $this->Flash->error('A Seção eleitoral ainda não está aberta.');
                                    $pode_votar = false;
                                } 
                                elseif(!is_null($eleitor->toArray()[0]->secao->fechamento) AND $agora > $eleitor->toArray()[0]->secao->fechamento)
                                {
                                    $this->Flash->error('A Seção eleitoral já está fechada.');
                                    $pode_votar = false;
                                }
                            }

                            if ($pode_votar)
                            {
                                // Verificar se o eleitor está LIBERADO PARA VOTAÇÃO
                                foreach ($eleitor as $e) {
                                    if ($e->status_id != 2)    
                                    {
                                        if ($e->status_id == 3) {
                                            $this->Flash->error('Eleitor já votou.');
                                            $this->Usuarios->Auditorias->add(['acao' => 'Aviso: eleitor que já votou tentou votar novamente.', 'usuario_id' => $e->id, 'secao_id' => $e->secao_id]); 
                                            $this->Auth->logout();
                                        }
                                        else {
                                            $this->Flash->error('Eleitor não está liberado para votação. Verifique junto a organização da eleição.');
                                            $this->Usuarios->Auditorias->add(['acao' => 'Aviso: eleitor não liberado para votação tentou votar.', 'usuario_id' => $e->id, 'secao_id' => $e->secao_id]); 
                                        }
                                        return $this->redirect(['action' => 'votarAutenticado']);
                                    }
                                    $tipo_id = $e->tipo_id; // Guardamos o tipo_id para consulta de candidatos
                                    $secao_id = $e->secao_id; // Guardamos a secao_id para gravar nos votos
                                    $eleitor_id = $e->id; // Guardamos o id do eleitor para alterar situação após registro dos votos
                                    $campus_id = $e->secao->campus_id; // Guardamos o campus para selecionar apenas candidatos do campus dele caso "permitir_votar_apenas_candidatos_do_mesmo_campus = true"

                                    if (!isset($this->request->data['confirmou_dados']))
                                    {
                                        // Registro de auditoria
                                        $this->Usuarios->Auditorias->add(['acao' => 'Eleitor liberado para votação entrou com seu CPF para votação.', 'usuario_id' => $e->id, 'secao_id' => $e->secao_id]); 
                                    }
                                }

                                // 1ª etapa - Confirmação do eleitor
                                // Repassamos as informações coletadas sobre o eleitor para que ele confirme:
                                $this->set(compact('eleitor'));

                                // 2ª etapa - eleitor clicou no botão "Sim, prosseguir"
                                if (isset($this->request->data['confirmou_dados']) AND $this->request->data['confirmou_dados'])
                                {
                                    if (!isset($this->request->data['Candidatos'])) {
                                        // Registro de auditoria
                                        $this->Usuarios->Auditorias->add(['acao' => 'Eleitor confirmou seus dados para a votação.', 'usuario_id' => $e->id, 'secao_id' => $e->secao_id]); 
                                    }

                                    /*** Carregar CANDIDATOS ***/
                                    $conditions = [];

                                    // Se a configuração do sistema permitir apenas o voto em candidatos do mesmo tipo
                                    if (parent::configuracao('permitir_votar_apenas_candidatos_do_mesmo_tipo') == 'true')
                                        $conditions['Candidatos.tipo_id'] = $tipo_id;

                                    // Se a configuração do sistema permitir apenas o voto em candidatos do mesmo campus
                                    if (parent::configuracao('permitir_votar_apenas_candidatos_do_mesmo_campus') == 'true')
                                        $conditions['Candidatos.campus_id'] = $campus_id;

                                    // Carregar candidatos
                                    $candidatos = $this->Votos->Candidatos->find('all', [
                                        'conditions' => $conditions,
                                        'contain' => ['Campi', 'Tipos'],
                                        'order' => ['Candidatos.nome']
                                    ]);

                                    // Se tem candidatos
                                    if($candidatos->count() > 0) {
                                        $this->set(compact('candidatos'));
                                        $lista_candidatos_votados_validos = array();

                                        // Se encaminhou votação
                                        if (isset($this->request->data['Candidatos'])) {
                                            foreach ($this->request->data['Candidatos'] as $candidato) {
                                                foreach ($candidatos as $candidato_valido) {
                                                    // Se o ID do candidato selecionado pertence a lista de candidatos aptos para o eleitor, guardamos
                                                    if ($candidato['id'] != 0 AND $candidato['id'] == $candidato_valido->id)
                                                        $lista_candidatos_votados_validos[] = $candidato['id']; 
                                                }
                                            }

                                            // Se selecionou até a quantidade máxima de candidatos que o eleitor pode votar
                                            if (count($lista_candidatos_votados_validos) <= $quantidade_candidatos_eleitor_pode_votar) 
                                            {
                                                $erro_ao_registrar_voto = false;
                                                $votos_registrados = array();

                                                // Reservei um código de votação
                                                $codigo = $this->Votos->reserva_codigo();

                                                // Se consegui reservar um código de votação
                                                if (!empty($codigo) AND $codigo['livre'])
                                                {
                                                    // Anulou o VOTO
                                                    if (isset($this->request->data['botao_anular']) AND $this->request->data['botao_anular'] == 1) 
                                                    {
                                                        // Verifico se é está configurado para permitir votos nulos
                                                        if ($permitir_nulo == 'true')
                                                        {
                                                            $voto = $this->Votos->newEntity();
                                                            $voto->candidato_id = null;
                                                            $voto->secao_id = $secao_id;
                                                            $voto->registro = date('Y-m-d H:i:s');
                                                            $voto->branco = null;
                                                            $voto->nulo = true;
                                                            $voto->codigo = $codigo['codigo'] . '-' . $codigo['dv'];
                                                            if (!$this->Votos->save($voto))
                                                                $erro_ao_registrar_voto = true;
                                                            else
                                                                $votos_registrados[] = $voto->id;
                                                        }
                                                        else {
                                                            $erro_ao_registrar_voto = true;
                                                            $this->Flash->error('Essa votação não permite voto nulo. Escolha um candidato.');        
                                                        }
                                                    }
                                                    // Votou em BRANCO
                                                    elseif (count($lista_candidatos_votados_validos) == 0) {
                                                        // Verifico se é está configurado para permitir votos brancos
                                                        if (parent::configuracao('permitir_branco') == 'true')
                                                        {
                                                            $voto = $this->Votos->newEntity();
                                                            $voto->candidato_id = null;
                                                            $voto->secao_id = $secao_id;
                                                            $voto->registro = date('Y-m-d H:i:s');
                                                            $voto->branco = true;
                                                            $voto->nulo = null;
                                                            $voto->codigo = $codigo['codigo'] . '-' . $codigo['dv'];
                                                            if (!$this->Votos->save($voto))
                                                                $erro_ao_registrar_voto = true;
                                                            else
                                                                $votos_registrados[] = $voto->id;
                                                        }
                                                        else {
                                                            $erro_ao_registrar_voto = true;
                                                            $this->Flash->error('Essa votação não permite voto em branco. Escolha um candidato.');        
                                                        }
                                                    }
                                                    else {
                                                        foreach ($lista_candidatos_votados_validos as $candidato_id) {
                                                            $voto = $this->Votos->newEntity();
                                                            $voto->candidato_id = $candidato_id;
                                                            $voto->secao_id = $secao_id;
                                                            $voto->registro = date('Y-m-d H:i:s');
                                                            $voto->branco = null;
                                                            $voto->nulo = null;
                                                            $voto->codigo = $codigo['codigo'] . '-' . $codigo['dv'];
                                                            if (!$this->Votos->save($voto))
                                                                $erro_ao_registrar_voto = true;
                                                            else
                                                                $votos_registrados[] = $voto->id;
                                                        }
                                                    }

                                                    // Se não houver erro ao registrar o voto, então alteramos a situação do eleitor
                                                    if (!$erro_ao_registrar_voto)
                                                    {
                                                        $usuario = $this->Usuarios->get($eleitor_id);
                                                        $usuario->status_id = 3; // Altero a situação do eleitor para JÁ VOTOU
                                                        if (!$this->Usuarios->save($usuario))
                                                        {
                                                            // Apago os votos registrados
                                                            foreach ($votos_registrados as $voto_registrado) {
                                                                $voto_para_apagar = $this->Votos->get($voto_registrado);
                                                                $this->Votos->delete($voto_para_apagar);
                                                            }
                                                            $this->Flash->error('Ocorreu um erro ao registrar os votos. Por gentileza procure o responsável pela Seção e informe o problema.');        
                                                            $this->Usuarios->Auditorias->add(['acao' => 'Erro: ao registrar os votos do eleitor.', 'usuario_id' => $e->id, 'secao_id' => $e->secao_id]); 
                                                            return $this->redirect(['action' => 'votarAutenticado']);
                                                        } else {
                                                            $this->Flash->success('<h3>Voto registrado com sucesso!</h3><p>Anote o seu número de protocolo:<br/><span style="color: red; font-size: 32px; font-weight: bold;">' . $codigo['codigo'] . '-' . $codigo['dv'] . '</span></p><p>Com ele é possível consultar o voto sem a identificação do eleitor, por isso guarde-o em segredo.</p><p><button class="success" id="btn_fechar">Clique para encerrar</button></p>');        
                                                            $this->Usuarios->Auditorias->add(['acao' => 'Eleitor votou com sucesso.', 'usuario_id' => $e->id, 'secao_id' => $e->secao_id]); 
                                                            return $this->redirect(['action' => 'votarAutenticado']);
                                                        }
                                                    } 
                                                    else 
                                                    {
                                                        // Apago os votos registrados
                                                        foreach ($votos_registrados as $voto_registrado) {
                                                            $voto_para_apagar = $this->Votos->get($voto_registrado);
                                                            $this->Votos->delete($voto_para_apagar);
                                                        }
                                                        // Registro de auditoria
                                                        $this->Usuarios->Auditorias->add(['acao' => 'Erro: ao registrar votos.', 'usuario_id' => $e->id, 'secao_id' => $e->secao_id]); 
                                                    }
                                                } else {
                                                    $this->Flash->error('Erro ao reservar código de votação. Por gentileza, comunique a organização da eleição.');        
                                                }
                                            } else
                                                $this->Flash->error('Só é permitida a votação em até ' . $quantidade_candidatos_eleitor_pode_votar . ' candidato(s).');        
                                        }
                                    } else
                                        $this->Flash->error('Não existem candidatos disponíveis para você votar. Qualquer dúvida procure a organização da eleição.');        
                                }
                            }
                        } 
                        else
                            $this->Flash->error('Seu CPF não está cadastrado no sistema! Verifique se você está cadastrado como eleitor. Entre em contato com a organização da eleição.');
                    } 
                    else
                        $this->Flash->error('O número do seu CPF não está cadastrado corretamente no Active Directory. Entre em contato com a organização da eleição.');
                }
            }
        }
    }

    public function contagemVotos()
    {
        // Alterar para layout do Eleitor
        $this->viewBuilder()->layout('eleitor');

        // Verificar se todas as seções já estão fechadas
        $secoes = $this->Votos->Secoes->find('list', [
            'conditions' => [
                'Secoes.fechamento IS NULL', 
                'Secoes.tipo IN' => ['sistema', 'autenticada'] // Via sistema ou autenticada pelo AD
            ],
            'fields' => ['Secoes.id', 'Secoes.nome']
        ]);

        if ($secoes->count() != 0)
        {
            $this->Flash->error('A contagem de votos só poderá ser realizada após o fechamento de todas as seções que estão operando via sistema. Ainda há ' . $secoes->count() . ' aberta(s).');
        }
        else
        {
            // Listar todas Sessões Fechadas 
            $secoes_fechadas = $this->Votos->Secoes->find('list', [
                'conditions' => ['Secoes.fechamento IS NOT NULL'],
                'fields' => ['Secoes.id', 'Secoes.nome'],
                'order' => 'Secoes.id'
            ]);

            // Relação de Candidatos e quantidade de votos, por seção eleitoral, incluindo brancos e nulos
            foreach ($secoes_fechadas as $secao_id => $secao_nome) 
            {
                $collection = $this->Votos->find('all', [
                    'conditions' => ['Votos.secao_id' => $secao_id]
                ]);

                $todos_votos_da_secao = new Collection($collection);

                $relacao_votos[$secao_id] = $todos_votos_da_secao->countBy(function($voto){
                    if (!is_null($voto->candidato_id))
                        return $voto->candidato_id;
                    elseif (!is_null($voto->branco))
                        return 'branco';
                    elseif (!is_null($voto->nulo))
                        return 'nulo';
                })->toArray();    
            }

            //arsort($relacao_votos); // ordeno o array de forma descrescente de contagem de votos

            $candidatos = $this->Votos->Candidatos->find('all', [
                'contain' => ['Campi'],
                'order' => ['Campi.nome', 'Candidatos.nome']
            ]);

            $this->set(compact(['relacao_votos','candidatos']));

            // Contagem por SEÇÃO e TIPO
            $contagem = ['total' => 0];
            $collection = $this->Votos->find('all', [
                'contain' => ['Candidatos']
            ]);

            $todos_votos = new Collection($collection);
            $votos_por_secao = $todos_votos->groupBy('secao_id');
            
            foreach ($votos_por_secao as $secao_id => $votos)
            {
                $votos2 = new Collection($votos);

                $count = $votos2->countBy(function($voto){
                    if (isset($voto->candidato))
                        return $voto->candidato->tipo_id;
                    else {
                        if ($voto->branco == true)
                            return 'branco';
                        elseif ($voto->nulo == true)
                            return 'nulo';
                    }
                });
                
                $contagem[$secao_id] = $count->toArray();
                $contagem[$secao_id]['sub-total'] = count($votos); // Guardo o sub-total por Seção
                $contagem['total'] += count($votos); // Atualizo o total de votos computados
            }
            //debug($contagem);
            $this->set(compact('contagem'));

            $secoes = $this->Votos->Secoes->find('all', ['order' => 'Secoes.id']);
            $tipos = $this->Votos->Candidatos->Tipos->find('list');
            $this->set(compact(['secoes','tipos']));

            // Contagem de quantos votos cada candidato recebeu por seção
            $contagem_por_candidato = array();
            $votos_por_candidato = $todos_votos->groupBy('candidato_id');

            foreach ($votos_por_candidato as $candidato_id => $votos)
            {
                $votos3 = new Collection($votos);
                $count = $votos3->countBy(function($voto){
                    return $voto->secao_id;
                });

                $contagem_por_candidato[$candidato_id] = $count->toArray();
            }

            //debug($contagem_por_candidato);
            $this->set(compact('contagem_por_candidato'));
        }
    }

    public function listaCodigos()
    {
        // Alterar para layout do Eleitor
        $this->viewBuilder()->layout('eleitor');

        // Verificar se todas as seções já estão fechadas
        $secoes_abertas = $this->Votos->Secoes->find('list', [
            'conditions' => [
                'Secoes.fechamento IS NULL', 
                'Secoes.tipo IN' => ['sistema', 'autenticada']
            ],
            'fields' => ['Secoes.id', 'Secoes.nome']
        ]);

        if ($secoes_abertas->count() != 0)
        {
            $this->Flash->error('A lista de códigos dos votos só poderá ser visualizada após o fechamento de todas as seções operadas via sistema. Ainda há ' . $secoes_abertas->count() . ' aberta(s).');
        }
        else
        {
            // Listar todas Sessões Fechadas 
            $secoes = $this->Votos->Secoes->find('list', [
                'conditions' => ['Secoes.fechamento IS NOT NULL'],
                'fields' => ['Secoes.id', 'Secoes.nome'],
                'order' => 'Secoes.id'
            ]);

            $votos = $this->Votos->find('all', [
                'order' => ['secao_id', 'codigo']
            ]);

            $votos = $this->Votos->loadInto($votos, ['Candidatos' => ['Campi', 'Tipos']]);

            $votos = new Collection($votos);

            $votos = $votos->groupBy('codigo');

            $this->set(compact('votos', 'secoes'));
        }
    }

    /**
     *  Consulta de Protocolo de votação
     */
    public function consultarProtocolo()
    {
        // Alterar para layout do Eleitor
        $this->viewBuilder()->layout('eleitor');

        // Se submeteu o código
        if (isset($this->request->data['codigo'])) {

            if (!empty($this->request->data['codigo']))
            {
                $this->loadComponent('Luhn');

                $protocolo = preg_replace("/[^0-9]/", "", $this->request->data['codigo']);
                
                $codigo[0] = substr($protocolo, 0, 4);
                $codigo[1] = substr($protocolo, 4, 1);

                // Se o código é formado por duas partes
                if (count($codigo) > 1)
                    $codigo_valido = $this->Luhn->validate($codigo[0], $codigo[1]);
                else
                    $codigo_valido = false;

                if ($codigo_valido)
                {
                    $voto = $this->Votos->find('all')->where(['codigo' => $codigo[0] . '-' . $codigo[1]]);

                    if (!empty($voto->toArray())) {
                        $voto = $this->Votos->loadInto($voto, ['Secoes' => ['Campi']]);
                        $voto = $this->Votos->loadInto($voto, ['Candidatos' => ['Campi', 'Tipos']]); // Carrego demais informações do Candidato
                        $this->set(compact('voto'));
                    }
                    else
                        $this->Flash->error('O código informado não foi utilizado durante a votação.');
                }
                else
                    $this->Flash->error('Código informado é inválido. Verifique se os dígitos foram digitados corretamente.');
            }
            else
                $this->Flash->error('Código informado é inválido. Verifique se os dígitos foram digitados corretamente.');
        }
    }

    /**
     * Teste de som da Urna Eletrônica
     */
    public function testeSom()
    {
        // Alterar para layout do Eleitor
        $this->viewBuilder()->layout('eleitor');
    }
}