<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Candidatos Controller
 *
 * @property \App\Model\Table\CandidatosTable $Candidatos
 */
class CandidatosController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Campi', 'Tipos']
        ];
        $this->set('candidatos', $this->paginate($this->Candidatos));
        $this->set('_serialize', ['candidatos']);
    }

    /**
     * View method
     *
     * @param string|null $id Candidato id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $candidato = $this->Candidatos->get($id, [
            'contain' => ['Campi', 'Tipos']
        ]);
        $this->set('candidato', $candidato);
        $this->set('_serialize', ['candidato']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $candidato = $this->Candidatos->newEntity();
        if ($this->request->is('post')) {
            $candidato = $this->Candidatos->patchEntity($candidato, $this->request->data);
            if ($this->Candidatos->save($candidato)) {
                $this->Flash->success(__('The candidato has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The candidato could not be saved. Please, try again.'));
            }
        }
        $campi = $this->Candidatos->Campi->find('list', ['limit' => 200]);
        $tipos = $this->Candidatos->Tipos->find('list', ['limit' => 200]);
        $this->set(compact('candidato', 'campi', 'tipos'));
        $this->set('_serialize', ['candidato']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Candidato id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $candidato = $this->Candidatos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $candidato = $this->Candidatos->patchEntity($candidato, $this->request->data);
            if ($this->Candidatos->save($candidato)) {
                $this->Flash->success(__('The candidato has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The candidato could not be saved. Please, try again.'));
            }
        }
        $campi = $this->Candidatos->Campi->find('list', ['limit' => 200]);
        $tipos = $this->Candidatos->Tipos->find('list', ['limit' => 200]);
        $this->set(compact('candidato', 'campi', 'tipos'));
        $this->set('_serialize', ['candidato']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Candidato id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $candidato = $this->Candidatos->get($id);
        if ($this->Candidatos->delete($candidato)) {
            $this->Flash->success(__('The candidato has been deleted.'));
        } else {
            $this->Flash->error(__('The candidato could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
