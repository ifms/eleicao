<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Configuracoes Controller
 *
 * @property \App\Model\Table\ConfiguracoesTable $Configuracoes
 */
class ConfiguracoesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('configuracoes', $this->paginate($this->Configuracoes));
        $this->set('_serialize', ['configuracoes']);
    }

    /**
     * View method
     *
     * @param string|null $id Configuracao id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $configuracao = $this->Configuracoes->get($id, [
            'contain' => []
        ]);
        $this->set('configuracao', $configuracao);
        $this->set('_serialize', ['configuracao']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $configuracao = $this->Configuracoes->newEntity();
        if ($this->request->is('post')) {
            $configuracao = $this->Configuracoes->patchEntity($configuracao, $this->request->data);
            if ($this->Configuracoes->save($configuracao)) {
                $this->Flash->success(__('The configuracao has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The configuracao could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('configuracao'));
        $this->set('_serialize', ['configuracao']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Configuracao id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $configuracao = $this->Configuracoes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $configuracao = $this->Configuracoes->patchEntity($configuracao, $this->request->data);
            if ($this->Configuracoes->save($configuracao)) {
                $this->Flash->success(__('The configuracao has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The configuracao could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('configuracao'));
        $this->set('_serialize', ['configuracao']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Configuracao id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $configuracao = $this->Configuracoes->get($id);
        if ($this->Configuracoes->delete($configuracao)) {
            $this->Flash->success(__('The configuracao has been deleted.'));
        } else {
            $this->Flash->error(__('The configuracao could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
