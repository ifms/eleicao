<div class="candidatos index large-9 medium-8 columns content">
    <?= $this->Html->link('Adicionar', ['action' => 'add'], ['class' => 'button success round tiny']) ?>
    <h3><?= __('Candidatos') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th style="width: 40px"><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th><?= $this->Paginator->sort('campus_id') ?></th>
                <th><?= $this->Paginator->sort('tipo_id') ?></th>
                <th style="width: 140px" class="actions"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($candidatos as $candidato): ?>
            <tr>
                <td><?= $this->Number->format($candidato->id) ?></td>
                <td><?= h($candidato->nome) ?></td>
                <td><?= $candidato->has('campus') ? $this->Html->link($candidato->campus->nome, ['controller' => 'Campi', 'action' => 'view', $candidato->campus->id]) : '' ?></td>
                <td><?= $candidato->has('tipo') ? $this->Html->link($candidato->tipo->nome, ['controller' => 'Tipos', 'action' => 'view', $candidato->tipo->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $candidato->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $candidato->id]) ?>
                    <?= $this->Form->postLink(__('Apagar'), ['action' => 'delete', $candidato->id], ['confirm' => __('Are you sure you want to delete # {0}?', $candidato->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
