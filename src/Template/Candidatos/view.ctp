<div class="candidatos view large-9 medium-8 columns content">
    <h3><?= h($candidato->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($candidato->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Campus') ?></th>
            <td><?= $candidato->has('campus') ? $this->Html->link($candidato->campus->nome, ['controller' => 'Campi', 'action' => 'view', $candidato->campus->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Tipo') ?></th>
            <td><?= $candidato->has('tipo') ? $this->Html->link($candidato->tipo->nome, ['controller' => 'Tipos', 'action' => 'view', $candidato->tipo->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($candidato->id) ?></td>
        </tr>
    </table>
</div>
