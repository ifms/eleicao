<div class="candidatos form large-9 medium-8 columns content">
    <?= $this->Form->create($candidato) ?>
    <fieldset>
        <legend><?= __('Add Candidato') ?></legend>
        <?php
            echo $this->Form->input('nome');
            echo $this->Form->input('campus_id', ['options' => $campi]);
            echo $this->Form->input('tipo_id', ['options' => $tipos]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
