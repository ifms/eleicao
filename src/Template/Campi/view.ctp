<div class="campi view large-9 medium-8 columns content">
    <h3><?= h($campus->nome) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($campus->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($campus->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Candidatos deste Campus') ?></h4>
        <?php if (!empty($campus->candidatos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Tipo') ?></th>
                <th class="actions"><?= __('Ações') ?></th>
            </tr>
            <?php foreach ($campus->candidatos as $candidatos): ?>
            <tr>
                <td><?= h($candidatos->id) ?></td>
                <td><?= h($candidatos->nome) ?></td>
                <td><?= h($candidatos->tipo->nome) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Candidatos', 'action' => 'view', $candidatos->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Candidatos', 'action' => 'edit', $candidatos->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Candidatos', 'action' => 'delete', $candidatos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $candidatos->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
