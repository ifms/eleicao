<div class="large-12 medium-12 columns">
	<?= $this->Form->create() ?>
	<fieldset>
        <legend>Entre com o número de protocolo mostrado ao final da votação:</legend>
       	<?= $this->Form->input('codigo', ['label' => false, 'maxlength' => 6,'id' => 'codigo', 'placeholder' => '____-_']); ?>
       	<?= $this->Form->button('Consultar', ['class' => 'success round']) ?>
		<?= $this->Form->end() ?>
    </fieldset> 
    <?
	if (isset($voto)) {
		echo '<fieldset>';
		echo '<h3>Informações do protocolo de votação:</h3>';
		echo '<table class="bordered striped">';
		echo '<tr>' .
				'<th>Seção</th>' .
				'<th class="text-center">Código</th>' .
				'<th class="text-center">Voto</th>' .
			'<tr>';
		foreach ($voto as $v) {
			echo '<tr>' .
					'<td>' . $v->secao_id . ' - ' . $v->secao->nome . ' (' . $v->secao->campus->nome . ')</td>' .
					'<td class="text-center"><strong>' . $v->codigo . '</strong></td>';
			if ($v->branco)
				echo '<td class="text-center">Branco</td>';
			elseif ($v->nulo)
				echo '<td class="text-center">Nulo</td>';
			elseif ($v->candidato_id)
				echo '<td class="text-center">' . $v->candidato->nome . ' (' . $v->candidato->tipo->nome .' - ' . $v->candidato->campus->nome . ')</td>';
			echo '</tr>';
		}
		echo '</table>';
		echo '</fieldset>';
	}
	?>
</div>	
<script type="text/javascript">
	$(document).ready(function() { 
		//$("#codigo").mask("9999-9", {placeholder:"____-_"}); // Desativado pois está apresentando conflito no Mobile
	});
</script>