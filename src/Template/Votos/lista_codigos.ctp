<style>
table * {
    font-size: 12px !important;
}

.bg_escuro td{
    background-color: #eee;
}
</style>
<div class="usuarios form large-12 medium-12 columns content">
    <h2>Códigos dos votos</h2>
    <?
    if (isset($votos) AND isset($secoes))
    {
        echo '<table class="bordered">';
        echo '<body>';
        $secao_atual = null;
        $votos = $votos->toArray();
        //debug($votos);

        foreach ($secoes as $secao_id => $secao) 
        {
            $qt_codigos_na_secao = 0;

            if ($secao_id != $secao_atual)
            {
                $secao_atual = $secao_id;
                echo '<tr><th colspan="4"><strong>SEÇÃO ' . $secao_atual . ' (' . $secao . ')</strong></th></tr>';
                echo '<tr>' .
                        '<td class="text-center"><strong>Número do Protocolo (Código do Voto)</strong></td>' .
                        '<td class="text-center"><strong>Candidato</strong></td>' .
                        '<td class="text-center"><strong>Campus</strong></td>' .
                        '<td class="text-center"><strong>Categoria</strong></td>' .
                    '</tr>';
            }

            foreach ($votos as $codigo => $voto) 
            {
                if ($voto[0]->secao_id == $secao_id)
                {
                    $class = ($qt_codigos_na_secao % 2 == 0 ) ? 'bg_escuro' : '';
                    //debug($voto[0]);
                    // Se o voto é formado por mais de um voto (quando existe possibilidade de votar em mais de um candidato)
                    if (count($voto) > 1)
                    {
                        echo '<tr class="' . $class . '">';
                        echo '<td class="text-center" rowspan="' . count($voto) . '"><strong>' . $codigo . '</strong></td>';
                        $i = 1;

                        foreach ($voto as $v) {
                            if ($i++ != 1)
                                echo '<tr class="' . $class . '">';

                            echo '<td class="text-center">' . $v->candidato->nome . '</td>';
                            echo '<td class="text-center">' . $v->candidato->campus->nome . '</td>';
                            echo '<td class="text-center">' . $v->candidato->tipo->nome . '</td>';
                            echo '</tr>';   
                        }                   
                    }
                    else
                    {
                        echo '<tr class="' . $class . '">';
                        echo '<td class="text-center"><strong>' . $codigo . '</strong></td>';
                        if (isset($voto[0]->candidato))
                        {
                            echo '<td class="text-center">' . $voto[0]->candidato->nome . '</td>';
                            echo '<td class="text-center">' . $voto[0]->candidato->campus->nome . '</td>';
                            echo '<td class="text-center">' . $voto[0]->candidato->tipo->nome . '</td>';
                        }
                        elseif ($voto[0]->branco)
                            echo '<td class="text-center" colspan="3">BRANCO</td>';
                        elseif ($voto[0]->nulo)
                            echo '<td class="text-center" colspan="3">NULO</td>';
                        echo '</tr>';   
                    }

                    $qt_codigos_na_secao++;   
                    unset($votos[$codigo]);
                }
            }
        }
        echo '</body>';
        echo '</table>';
    }
    ?>
</div>
<div class="large-12 medium-12 columns content">
    <?= $this->Html->link('Voltar', '/', ['class' => 'button warning round']) ?>
</div>