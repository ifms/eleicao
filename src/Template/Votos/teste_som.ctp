<div class="large-12 medium-12 columns content">
  	<h2>Teste de Audio para Urna</h2>
	<button class="button round success ok">Voto Ok</button>

	<button class="button round warning erro">Erro ao votar</button>
	<audio id="voto_ok" preload="auto">
	  <source src="./audio/voto_ok.mp3" type="audio/mpeg"> Seu navegador não Seu navegador não suporta som.
	</audio>
	<audio id="voto_erro" preload="auto">
	  <source src="./audio/voto_erro.mp3" type="audio/mpeg"> Seu navegador não Seu navegador não suporta som.
	</audio>
</div>
<script type="text/javascript">
	$(document).ready(function() { 
        $('button.ok').click(function() {
        	$('#voto_ok')[0].play();
        });

        $('button.erro').click(function() {
        	$('#voto_erro')[0].play();
        });
	});
</script>