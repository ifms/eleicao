<div class="votos view large-9 medium-8 columns content">
    <h3><?= h($voto->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Candidato') ?></th>
            <td><?= $voto->has('candidato') ? $this->Html->link($voto->candidato->id, ['controller' => 'Candidatos', 'action' => 'view', $voto->candidato->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Secao') ?></th>
            <td><?= $voto->has('secao') ? $this->Html->link($voto->secao->id, ['controller' => 'Secoes', 'action' => 'view', $voto->secao->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($voto->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Registro') ?></th>
            <td><?= h($voto->registro) ?></td>
        </tr>
        <tr>
            <th><?= __('Branco') ?></th>
            <td><?= $voto->branco ? __('Yes') : __('No'); ?></td>
         </tr>
        <tr>
            <th><?= __('Nulo') ?></th>
            <td><?= $voto->nulo ? __('Yes') : __('No'); ?></td>
         </tr>
    </table>
</div>
