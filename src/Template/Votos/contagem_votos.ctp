<style>
table * {
    font-size: 12px !important;
}
</style>
<div class="usuarios form large-12 medium-12 columns content">
    <h2>Contagem de Votos</h2>
    <? if (isset($contagem)): ?>
        <h4>Resumo dos votos</h4>
        <?  if (count($contagem) > 0): ?>
                <table class="striped bordered hover stack">
                    <thead>
                        <tr>
                            <td style="width: 180px"></td>
                            <th class="text-center" colspan="<?= $secoes->count() ?>">SEÇÕES ELEITORAIS</th>
                            <td style="width: 120px"></td>
                        </tr>
                        <tr>
                            <td></td>
                            <? foreach ($secoes as $secao): ?>
                                <th class="text-center"><?= $secao->id ?></th>
                            <? endforeach; ?>
                            <td class="text-center">Total por Categoria</td>
                        </tr>
                    </thead>
                    <tbody>
                    <?
                        $tipos_resumo = $tipos->toArray();
                        $tipos_resumo += ['branco' => 'Branco', 'nulo' => 'Nulo', 'sub-total' => 'Total por Seção'];
                        //debug($tipos);
                        //debug($contagem);
                        $qt_votos_por_tipo = [];

                        foreach ($tipos_resumo as $tipo_id => $tipo): ?>
                        <tr>
                            <td><b><?= $tipo ?></b></td>
                            <? foreach ($secoes as $secao): ?>
                                <td class="text-center"><?
                                    $qt_votos = isset($contagem[$secao->id][$tipo_id]) ? $contagem[$secao->id][$tipo_id] : 0;
                                    if (!isset($qt_votos_por_tipo[$tipo_id]))
                                        $qt_votos_por_tipo[$tipo_id] = $qt_votos;
                                    else
                                        $qt_votos_por_tipo[$tipo_id] += $qt_votos;

                                    if ($tipo_id == 'sub-total')
                                        echo "<b>$qt_votos</b>";
                                    else
                                        echo $qt_votos;
                                    ?>
                                </td>
                            <? endforeach; ?>
                            <td class="text-center"><b><?= $qt_votos_por_tipo[$tipo_id] ?></b></td>
                        </tr>
                    <? endforeach; ?>
                    </tbody>
                </table>
        <? else: ?>
            <p>Nenhum voto computado.</p>
        <? endif; ?>
    <? endif; ?>
</div>
<? if (isset($relacao_votos)): ?>
    <div class="large-12 medium-12 columns content">
        <h4>Quantidade de votos por Candidato por Categoria</h4>
        <? if (count($relacao_votos) > 0) : ?>
            <? foreach ($tipos as $tipo_id => $tipo) : ?>
                <h5><?= $tipo ?></h5>
                <table class="striped bordered">
                    <thead>
                        <tr>
                            <td></td>
                            <th class="text-center" colspan="<?= $secoes->count() ?>">SEÇÕES ELEITORAIS</th>
                            <td style="width: 120px"></td>
                        </tr>
                        <tr>
                            <td>NOME DO CANDIDATO / CAMPUS</td>
                            <? foreach ($secoes as $secao): ?>
                            <th class="text-center"><?= $secao->id ?></th>    
                            <? endforeach; ?> 
                            <td class="text-center">VOTOS REGISTRADOS</td>
                        </tr>
                    </thead>
                    <tbody>
                    <? 
                        $qt_candidatos_nessa_tabela = 0;
                        $total_por_secao = [];
                        $total_geral = 0;

                        foreach ($candidatos as $candidato) 
                        {
                            if ($candidato->tipo_id == $tipo_id)
                            {
                                $qt_candidatos_nessa_tabela++;
                                ?>
                                <tr>
                                    <td style="width: 180px !important"><?=  $candidato->nome . ' (' . $candidato->campus->nome . ')' ?></td>
                                    <?
                                    $total = 0;
                                    foreach ($secoes as $secao)
                                    { 
                                        if (isset($relacao_votos[$secao->id][$candidato->id]))
                                        {
                                            $total += $relacao_votos[$secao->id][$candidato->id];
                                            if (isset($total_por_secao[$secao->id]))
                                                $total_por_secao[$secao->id] += $relacao_votos[$secao->id][$candidato->id];
                                            else
                                                $total_por_secao[$secao->id] = $relacao_votos[$secao->id][$candidato->id];

                                            echo '<td class="text-center" style="width: 10px !important">' . $relacao_votos[$secao->id][$candidato->id] . '</td>';
                                        }
                                        else
                                        {
                                            if (isset($total_por_secao[$secao->id]))
                                                $total_por_secao[$secao->id] += 0;
                                            else
                                                $total_por_secao[$secao->id] = 0;

                                            echo '<td class="text-center" style="width: 10px !important">' . 0 . '</td>';
                                        }
                                        ?>
                                    <?
                                    } ?>
                                    <td class="text-center"><?= $total ?></td>
                                    <?
                                    $total_geral += $total;
                                    ?>
                                </tr>
                            <?
                            }
                        }
                        if ($qt_candidatos_nessa_tabela == 0) { 
                            if (!in_array($tipo_id, ['branco', 'nulo', 'sub-total']))
                            {
                            ?>
                                <td colspan="<?= 2 + $secoes->count() ?>" class="text-center"><span style="color: red">Nenhum candidato registrado para esta categoria.</span></td>
                            <?php
                            }
                        }
                        else {
                            // SUBTOTAL ?>
                            <tr>
                                <td style="width: 180px !important"><strong>SUB-TOTAL</strong></td>
                                <?
                                $total = 0;
                                foreach ($secoes as $secao)
                                { 
                                    echo '<td class="text-center" style="width: 10px !important"><strong>' . $total_por_secao[$secao->id] . '</strong></td>';
                                } 
                                ?>
                                <td class="text-center"><strong><?= $total_geral ?></strong></td>
                            </tr>
                        <?
                        }
                        ?>
                    </tbody>
                </table>
            <? endforeach; ?>
        <? endif; ?>
    </div>
    <div class="large-12 medium-12 columns content">
        <? if (isset($relacao_votos) AND count($relacao_votos) > 0) : ?>
        <h4>Quantidade de votos Brancos e Nulos por Seção</h4>
        <table class="striped bordered">
            <thead>
                <tr>
                    <td></td>
                    <th class="text-center" colspan="<?= $secoes->count() ?>">SEÇÕES ELEITORAIS</th>
                    <td style="width: 120px"></td>
                </tr>
                <tr>
                    <td></td>
                    <? foreach ($secoes as $secao): ?>
                    <th class="text-center"><?= $secao->id ?></th>    
                    <? endforeach; ?> 
                    <td class="text-center">VOTOS REGISTRADOS</td>
                </tr>
            </thead>
            <tbody>
            <? 
                $total_brancos_nulos = 0;
                $total_por_secao = [];
                $brancos_nulos = ['branco' => 'Brancos', 'nulo' => 'Nulos'];
                foreach ($brancos_nulos as $tipo_id => $tipo)
                { ?>
                    <tr>
                        <td style="width: 180px !important"><strong><?=  $tipo ?></strong></td>
                        <?
                        $total = 0;
                        foreach ($secoes as $secao)
                        { 
                            if (isset($relacao_votos[$secao->id][$tipo_id]))
                            {
                                $total += $relacao_votos[$secao->id][$tipo_id];
                                if (isset($total_por_secao[$secao->id]))
                                    $total_por_secao[$secao->id] += $relacao_votos[$secao->id][$tipo_id];
                                else
                                    $total_por_secao[$secao->id] = $relacao_votos[$secao->id][$tipo_id];

                                echo '<td class="text-center" style="width: 10px !important">' . $relacao_votos[$secao->id][$tipo_id] . '</td>';
                            }
                            else
                            {
                                if (isset($total_por_secao[$secao->id]))
                                    $total_por_secao[$secao->id] += 0;
                                else
                                    $total_por_secao[$secao->id] = 0;

                                echo '<td class="text-center" style="width: 10px !important">' . 0 . '</td>';
                            }
                            ?>
                        <?
                        } ?>
                        <td class="text-center"><?= $total ?></td>
                        <?
                        $total_brancos_nulos += $total;
                        ?>
                    </tr>
                <?
                }
                // SUBTOTAL ?>
                <tr>
                    <td style="width: 180px !important"><strong>SUB-TOTAL</strong></td>
                    <?
                    $total = 0;
                    foreach ($secoes as $secao)
                    { 
                        echo '<td class="text-center" style="width: 10px !important"><strong>' . $total_por_secao[$secao->id] . '</strong></td>';
                    } 
                    ?>
                    <td class="text-center"><strong><?= $total_brancos_nulos ?></strong></td>
                </tr>
            </tbody>
        </table>
        <? endif; ?>
    </div>
<? endif; ?>
<div class="large-12 medium-12 columns content">
<? if (isset($secoes) AND $secoes->count() > 0): ?>
    <h6>Legenda das Seções:</h6>
    <table class="striped large-6">
        <thead>
            <tr>
                <th class="large-2">Número</th>
                <th>Seção</th>
                <th>Abertura</th>
                <th>Fechamento</th>
            </tr>
        </thead>
        <tbody>
        <? foreach ($secoes as $secao): ?>
            <tr>
                <td><strong><?= $secao->id ?></strong></td>
                <td><?= $secao->nome ?></td>
                <td><?= $secao->abertura ?></td>
                <td><?= $secao->fechamento ?></td>
            </tr>
        <? endforeach; ?> 
        </tbody>
    </table>
</div>
<? endif; ?>
<div class="large-12 medium-12 columns content">
    <?= $this->Html->link('Voltar', '/', ['class' => 'button warning round']) ?>
</div>