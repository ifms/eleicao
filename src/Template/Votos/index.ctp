<div class="votos index large-9 medium-8 columns content">
    <?= $this->Html->link('Adicionar', ['action' => 'add'], ['class' => 'button success round tiny']) ?>
    <h3><?= __('Votos') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('candidato_id') ?></th>
                <th><?= $this->Paginator->sort('secao_id') ?></th>
                <th><?= $this->Paginator->sort('registro') ?></th>
                <th><?= $this->Paginator->sort('branco') ?></th>
                <th><?= $this->Paginator->sort('nulo') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($votos as $voto): ?>
            <tr>
                <td><?= $this->Number->format($voto->id) ?></td>
                <td><?= $voto->has('candidato') ? $this->Html->link($voto->candidato->id, ['controller' => 'Candidatos', 'action' => 'view', $voto->candidato->id]) : '' ?></td>
                <td><?= $voto->has('secao') ? $this->Html->link($voto->secao->id, ['controller' => 'Secoes', 'action' => 'view', $voto->secao->id]) : '' ?></td>
                <td><?= h($voto->registro) ?></td>
                <td><?= h($voto->branco) ?></td>
                <td><?= h($voto->nulo) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $voto->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $voto->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $voto->id], ['confirm' => __('Are you sure you want to delete # {0}?', $voto->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
