<div class="votos form large-9 medium-8 columns content">
    <?= $this->Form->create($voto) ?>
    <fieldset>
        <legend><?= __('Edit Voto') ?></legend>
        <?php
            echo $this->Form->input('candidato_id', ['options' => $candidatos, 'empty' => true]);
            echo $this->Form->input('secao_id', ['options' => $secoes]);
            echo $this->Form->input('registro');
            echo $this->Form->input('branco');
            echo $this->Form->input('nulo');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
