<?
if (isset($eleitor) AND !isset($candidatos)) {
	?>
	<div class="usuarios form large-12 medium-12 columns content">
    	<? foreach ($eleitor as $eleitor): ?>
    		<fieldset>
        		<legend>Confirmação dos dados do eleitor:</legend>
        		<p>Por gentileza, confirme se os dados abaixo estão corretos e pertencem a você:</p>
        		<div>
					<div class="medium-2 large-1 columns">Nome:</div>
					<div class="medium-10 large-11 columns"><b><?= $eleitor->nome ?></b></div>
				</div>
				<div>
					<div class="medium-2 large-1 columns">CPF:</div>
					<div class="medium-10 large-11 columns"><b><?= $eleitor->cpf ?></b></div>
				</div>
				<div>
					<div class="medium-2 large-1 columns">Categoria:</div>
					<div class="medium-10 large-11 columns"><b><?= $eleitor->tipo->nome ?></b></div>
				</div>
				<div>
					<div class="medium-2 large-1 columns">Seção:</div>
					<div class="medium-10 large-11 columns"><b><?= $eleitor->secao->id . ' - ' . $eleitor->secao->nome ?></b></div>
				</div>
	    	</fieldset>	
		    <div>
				<div class="medium-12 large-12 columns">
			    <?
					echo $this->Form->postButton('Sim, prosseguir',[  
		                    'controller' => 'Votos', 
		                    'action' => 'votar'
		                ],[
		                    'data' => [
		                        'cpf' => $eleitor->cpf,
		                        'confirmou_dados' => true
		                    ],
		                    'class' => 'success round'
		                ]
		            );
		            echo ' ' . $this->Form->button('Não',[
		                    'class' => 'alert round',
		                    'id' => 'btn_nao'
		                ]
		            );
		        ?>
	        	</div>
	        </div>
		<? endforeach; ?>		        
	</div>
    <?
} elseif (isset($eleitor) AND isset($candidatos)) { 
	?>
	<div class="votar form large-12 medium-12 columns content">
	<?
	echo $this->Form->create('Candidatos');
	foreach ($eleitor as $eleitor):
		echo $this->Form->input('cpf', ['value' => $eleitor->cpf, 'type' => 'hidden']);
	endforeach;
	echo $this->Form->input('confirmou_dados', ['value' => true, 'type' => 'hidden']);
	?>
	<fieldset>
		<legend>Votação:</legend>
		<p>Selecione até <?= $quantidade_candidatos_eleitor_pode_votar ?> candidato(s) que deseja votar e clique em "Confirmar".</p>
		<table>
			<thead>
				<tr>
					<th style="width: 45px"></th>
					<th>Candidato</th>
					<th>Campus</th>
				</tr>
			</thead>
			<tbody>
			<?
			foreach ($candidatos as $i => $candidato) : ?>
				<tr>
					<td><?= $this->Form->input("Candidatos.$i.id", ['type' => 'checkbox', 'label' => false, 'value' => $candidato->id]) ?></td>
					<td><?= $candidato->nome ?></td>
					<td><?= $candidato->campus->nome ?></td>
				</tr>
			<? endforeach; ?>
			<tbody>
		</table>
	</fieldset>
	<?
	echo $this->Form->button('Confirmar', ['name' => 'botao_confirmar','class' => 'success round', 'type' => 'submit', 'value' => true]);
	echo $this->Form->button('Corrigir', ['class' => 'warning round','id' => 'botao_reset']);
	if ($permitir_nulo == 'true')
		echo $this->Form->button('Anular Voto', ['name' => 'botao_anular','class' => 'alert round', 'value' => true]);
	echo $this->Form->end(); ?>
	</div>
<?
} else {
	?>
	<div class="usuarios form large-12 medium-12 columns content">
		<?= $this->Form->create() ?>
		<fieldset>
	        <legend>Entre com o número do seu CPF (somente números):</legend>
	       	<?= $this->Form->input('cpf', ['label' => false, 'maxlength' => 11,'id' => 'cpf_votar']); ?>
	    </fieldset>
	    <?= $this->Form->button('Entrar para votar', ['class' => 'warning round']) ?>
		<?= $this->Form->end() ?>
	</div>	
<? 
} ?>
<div id="mensagem_erro_dados" class="hidden">
	<div class="small-10 columns">
		<p class="mensagem_uiblock">Informe a divergência nos dados ao responsável pela Seção eleitoral.</p>
	</div>
	<div class="small-2 columns">
  		<a class="close_uiblock" title="Clique para fechar">&#215;</a>
  	</div>
</div>

<div id="mensagem_sucesso" class="hidden">
	<div class="small-10 columns">
		<p class="mensagem_uiblock">Registrando o voto... Aguarde.</p>
	</div>
</div>
<audio id="voto_ok" preload="auto">
  <source src="./audio/voto_ok.mp3" type="audio/mpeg"> Seu navegador não suporta som.
</audio>
<audio id="voto_erro" preload="auto">
  <source src="./audio/voto_erro.mp3" type="audio/mpeg"> Seu navegador não suporta som.
</audio>
<script type="text/javascript">
	$(document).ready(function() { 
		$('#btn_nao').click(function() { 
			$('#voto_erro')[0].play();
	        $.blockUI({ message: $('#mensagem_erro_dados') });
	    }); 

	    $('.close_uiblock').click(function(e){
	    	e.preventDefault();
	    	$(location).attr('href', './votar');
	    	$.unblockUI(); 
	    });

	    if ($('div.message').length && $('div.error').length) {
	    	$('#mensagem_sucesso').html($('div.message').text());
	    	$('div.message').hide();
	    	$.blockUI({ 
	    		message: $('#mensagem_sucesso'),
	    		css: {
	    			padding: '20px',
	    			backgroundColor: '#C3232D',
    				color: '#FFF',
    				border: 'none',
    				fontSize: '18px'
	    		},
	    		onOverlayClick: $.unblockUI
	    	});

	    	setTimeout(function() { 
	            $.unblockUI(); 
	        }, 5000);
	        $('#voto_erro')[0].play();
	    }
	    
	    if ($('div.message').length && $('div.success').length) {
	    	$('#mensagem_sucesso').html($('div.message').text());
	    	$('div.message').hide();
	    	$.blockUI({ 
	    		message: $('#mensagem_sucesso'),
	    		css: {
	    			padding: '20px 20px 0px 20px'
	    		}
	    	});
	     // setTimeout(function() { 
	     //        $.unblockUI({ 
	     //            onUnblock: function(){ 
	     //            	if (!$('.error').length) {
	     //            		$(location).attr('href', './votar');
	     //            	}
	     //            } 
	     //        }); 
	     //    }, 3000);
	    }
		
	    $('#botao_reset').click(function(e){
	    	e.preventDefault();
	    	$('input:checkbox').attr("checked", false);
	    });

	    $('#btn_fechar').click(function(){
	    	$('#voto_ok')[0].play();
	    	$.unblockUI({ 
                onUnblock: function(){ 
                	if (!$('.error').length) {
                		$(location).attr('href', './votar');
                	}
                } 
            });
	    });
	});
</script>