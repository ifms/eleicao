<?
if (isset($eleitor) AND !isset($candidatos)) {
	?>
	<div class="usuarios form large-12 medium-12 columns content">
    	<? foreach ($eleitor as $eleitor): ?>
    		<fieldset>
        		<legend>Confirmação dos dados do eleitor:</legend>
        		<p>Por gentileza, confirme se os dados abaixo estão corretos e pertencem a você:</p>
        		<div>
					<div class="medium-2 large-1 columns">Nome:</div>
					<div class="medium-10 large-11 columns"><b><?= $eleitor->nome ?></b></div>
				</div>
				<div>
					<div class="medium-2 large-1 columns">CPF:</div>
					<div class="medium-10 large-11 columns"><b><?= $eleitor->cpf ?></b></div>
				</div>
				<div>
					<div class="medium-2 large-1 columns">Categoria:</div>
					<div class="medium-10 large-11 columns"><b><?= $eleitor->tipo->nome ?></b></div>
				</div>
				<div>
					<div class="medium-2 large-1 columns">Seção:</div>
					<div class="medium-10 large-11 columns"><b><?= $eleitor->secao->id . ' - ' . $eleitor->secao->nome ?></b></div>
				</div>
	    	</fieldset>	
		    <div>
				<div class="medium-12 large-12 columns">
			    <?
					echo $this->Form->create();
					echo '<br>' . $this->Form->postButton('Sim, prosseguir',[  
		                    'controller' => 'Votos', 
		                    'action' => 'votar-autenticado'
		                ],[
		                    'data' => [
		                        'confirmou_dados' => true
		                    ],
		                    'class' => 'success round'
		                ]
		            );
		            echo ' ' . $this->Form->button('Não',[
		                    'class' => 'alert round',
		                    'id' => 'btn_nao'
		                ]
					);
					echo $this->Form->end();
		        ?>
	        	</div>
	        </div>
		<? endforeach; ?>		        
	</div>
    <?
} elseif (isset($eleitor) AND isset($candidatos)) { 
	?>
	<div class="votar form large-12 medium-12 columns">
		<?
		echo $this->Form->create('Candidatos', ['url' => ['controller' => 'Votos', 'action' => 'votar-autenticado']]);
		echo $this->Form->input('confirmou_dados', ['value' => true, 'type' => 'hidden']);
		?>
		<fieldset>
			<legend>Votação:</legend>
			<p>Selecione até <?= $quantidade_candidatos_eleitor_pode_votar ?> candidato(s) que deseja votar e clique em "Confirmar".</p>
			<table>
				<thead>
					<tr>
						<th style="width: 45px"></th>
						<th>Candidato</th>
						<th>Campus</th>
					</tr>
				</thead>
				<tbody>
				<?
				foreach ($candidatos as $i => $candidato) : ?>
					<tr>
						<td><?= $this->Form->input("Candidatos.$i.id", ['type' => 'checkbox', 'label' => false, 'value' => $candidato->id]) ?></td>
						<td><?= $candidato->nome ?></td>
						<td><?= $candidato->campus->nome ?></td>
					</tr>
				<? endforeach; ?>
				<tbody>
			</table>
		</fieldset>
		<?
		echo $this->Form->button('Confirmar', ['name' => 'botao_confirmar','class' => 'success round', 'type' => 'submit', 'value' => true]);
		echo $this->Form->button('Corrigir', ['class' => 'warning round','id' => 'botao_reset']);
		if ($permitir_nulo == 'true')
			echo $this->Form->button('Anular Voto', ['name' => 'botao_anular','class' => 'alert round', 'value' => true]);
		echo $this->Form->end(); ?>
	</div>
<?
} else {
	?>
	<div class="small-12 medium-4 large-4 columns">
		<fieldset>
			<p>
				<?= $descricao_eleicao; ?> (<?= $this->Html->link('mais informações', $url_edital, ['class' => '', 'title' => 'Clique para consultar o protocolo/número gerado após a sua votação.']); ?>).
			</p>
		</fieldset>
	</div>
	<div class="small-12 medium-4 large-4 columns">
		<fieldset>
			<legend>Votar</legend>
			<p>Para votar entre com sua matrícula SIAPE e sua senha do SUAP</p>
			<?= $this->Form->create() ?>
			<?= $this->Form->input('siape', ['label' => 'SIAPE (somente números)', 'placeholder' => 'SIAPE']); ?>
			<?= $this->Form->input('senha', ['type' => 'password', 'placeholder' => '******']) ?>
			<?= $this->Form->button(__('Entrar usando SUAP'), ['class' => 'button success radius', 'style' => 'margin-left: 0', 'title' => 'Clique para entrar']); ?>
			<?= $this->Form->end() ?>
		</fieldset>
	</div>
	<div class="small-12 medium-4 large-4 columns">
		<fieldset>
			<legend>Consultar protocolo de votação</legend>
			<p>Após votar o sitema exibe um código formado por 5 números, chamado de protocolo de votação. Com ele é possível consultar o voto.</p>
			<?= $this->Html->link('Consultar', ['controller' => 'votos', 'action' => 'consultarProtocolo'], ['class' => 'button small info radius', 'title' => 'Clique para consultar o protocolo/número gerado após a sua votação.']); ?>
		</fieldset>
	</div>	
<? 
} ?>
<div id="mensagem_erro_dados" class="hidden">
	<div class="small-10 columns">
		<p class="mensagem_uiblock">Informe a divergência nos dados a organização da eleição.</p>
	</div>
	<div class="small-2 columns">
  		<a class="close_uiblock" title="Clique para fechar">&#215;</a>
  	</div>
</div>

<div id="mensagem_sucesso" class="hidden">
	<div class="small-10 columns">
		<p class="mensagem_uiblock">Registrando o voto... Aguarde.</p>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() { 
		$('#btn_nao').click(function() { 
	        $.blockUI({ 
				message: $('#mensagem_erro_dados'),
				css: {
					padding: '20px',
                	left: 0,
                	width: '100%'
				}
			});
	    }); 

	    $('.close_uiblock').click(function(e){
	    	e.preventDefault();
	    	$(location).attr('href', '<?php echo $this->Url->build(['controller' => 'Usuarios', 'action' => 'logout']); ?>');
	    	$.unblockUI(); 
	    });

	    if ($('div.message').length && $('div.error').length) {
	    	$('#mensagem_sucesso').html($('div.message').text());
	    	$('div.message').hide();
	    	$.blockUI({ 
	    		message: $('#mensagem_sucesso'),
	    		css: {
	    			padding: '20px',
	    			backgroundColor: '#C3232D',
    				color: '#FFF',
    				border: 'none',
    				fontSize: '18px',
					left: 0,
                	width: '100%'
	    		},
	    		onOverlayClick: $.unblockUI
	    	});

	    	setTimeout(function() { 
	            $.unblockUI(); 
	        }, 5000);
	    }
	    
	    if ($('div.message').length && $('div.success').length) {
	    	$('#mensagem_sucesso').html($('div.message').text());
	    	$('div.message').hide();
	    	$.blockUI({ 
	    		message: $('#mensagem_sucesso'),
	    		css: {
	    			padding: '20px 20px 0px 20px',
					top: 0,
					left: 0,
                	width: '100%'
	    		}
	    	});
	    }
		
	    $('#botao_reset').click(function(e){
	    	e.preventDefault();
	    	$('input:checkbox').attr("checked", false);
	    });

	    $('#btn_fechar').click(function(){
	    	$.unblockUI({ 
                onUnblock: function(){ 
                	if (!$('.error').length) {
                		$(location).attr('href', '<?php echo $this->Url->build(['controller' => 'Usuarios', 'action' => 'logout']); ?>');
                	}
                } 
            });
	    });
	});
</script>