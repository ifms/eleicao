<?
if (isset($secao))
{
    $declaracao = "<p>Eu <b>$user[nome]</b>, Mesário, declaro fechada a <b>Seção $secao->id - $secao->nome</b> a partir de: " . date('d/m/Y à\s H:i:s') . " (horário local).</p><p>Abaixo o relatório de votos desta seção neste momento:</p>";
    $declaracao .= '<table border="1" style="border-collapse: collapse;" cellpadding="2">
                        <thead>
                            <tr>
                                <th>Candidato (Tipo)</th>
                                <th>Campus</th>
                                <th>Quantidade de votos nesta seção</th>
                            </tr>
                        </thead>
                        <tbody>';
    if (isset($candidatos)) {
        $total_votos_em_candidatos = 0;
        foreach ($candidatos as $candidato) {
            $qt_votos = (empty($votos_por_candidato[$candidato->id]) ? 0 : $votos_por_candidato[$candidato->id]);
            $declaracao .= '<tr>' .
                            "<td>$candidato->nome (" . $candidato->tipo->nome . ")</td>" .
                            "<td>" . $candidato->campus->nome . "</td>" .
                            "<td>$qt_votos</td>" .
                        '</tr>';
            $total_votos_em_candidatos += $qt_votos;
        }
        $declaracao .= '<tr>' .
                            "<td colspan='2'><b>Subtotal</b></td>" .
                            "<td><b>$total_votos_em_candidatos</b></td>" .
                        '</tr>' .
                        '<tr>' .
                            "<td colspan='2'>Votos em branco</td>" .
                            "<td>" . (empty($votos_brancos_nulos['branco']) ? '0' : $votos_brancos_nulos['branco']) . "</td>" .
                        '</tr>' .
                        '<tr>' .
                            "<td colspan='2'>Votos nulos</td>" .
                            "<td>" . (empty($votos_brancos_nulos['nulo']) ? '0' : $votos_brancos_nulos['nulo']) . "</td>" .
                        '</tr>' .
                        '<tr>' .
                            "<td colspan='2'><b>TOTAL</b></td>" .
                            "<td><b>$total_votos</b></td>" .
                        '</tr>';
    }
    $declaracao .= '</tbody></table><br/><br/>';

    $declaracao .= '<table border="1" style="border-collapse: collapse;" cellpadding="2">
                        <thead>
                            <tr>
                                <th rowspan="2">Categoria</th>
                                <th colspan="3"><center>Quantidade de Eleitores Aptos</center></th>
                                <th><center>Quantidade de Eleitores Não Aptos</center></th>
                            </tr>
                            <tr>';
                                foreach ($status as $s)
                                    $declaracao .= '<th>' . $s . '</th>';
                                
            $declaracao .= '</tr>
                        </thead>
                        <tbody>';
    if (isset($tipos)) {
        $total = array();
        foreach ($tipos as $tipo_id => $tipo) {
            
            $declaracao .= '<tr>' .
                            '<th>' . $tipo . '</td>';
                            foreach ($status as $status_id => $s)
                            {
                                $qt = isset($qt_eleitores[$status_id][$tipo_id]) ? $qt_eleitores[$status_id][$tipo_id] : 0;
                                if (isset($total[$status_id]))
                                    $total[$status_id] += $qt;
                                else
                                    $total[$status_id] = $qt;
                                $declaracao .= '<td>' . $qt . '</td>';
                            }
            $declaracao .= '</tr>';
        }
        $declaracao .= '<tr>' .
                        '<td><b>TOTAL</b></td>';
                        foreach ($status as $status_id => $s)
                            $declaracao .= '<td><b>' . $total[$status_id] . '</b></td>';
        $declaracao .= '</tr>';
    }
    $declaracao .= '</tbody></table>';

    ?>
    <div class="secoes form large-9 medium-8 columns content">
        <?= $this->Form->create($secao) ?>
        <fieldset>
            <legend>Fechar Seção</legend>
            <?php
                echo $this->Form->hidden('id');
                echo $this->Form->hidden('declaracao', ['value' => $declaracao]);
                echo $declaracao;
            ?>
        </fieldset>
        <?= $this->Form->button('Fechar', ['class' => 'button small round success']) ?>
        <?= $this->Form->end() ?>
    </div>
<?
}
?>