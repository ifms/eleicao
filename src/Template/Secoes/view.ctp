<div class="secoes view large-9 medium-8 columns content">
    <h3><?= h($secao->nome) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($secao->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($secao->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Campus Id') ?></th>
            <td><?= $secao->campus->nome ?></td>
        </tr>
    </table>
</div>
