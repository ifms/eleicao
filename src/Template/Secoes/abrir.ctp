<?
if (isset($secao))
{
    $declaracao = "<p>Eu <b>$user[nome]</b>, Mesário, declaro aberta a <b>Seção $secao->id - $secao->nome</b> a partir de: " . date('d/m/Y à\s H:i:s') . " (horário local).</p><p>Abaixo o relatório de votos desta seção neste momento:</p>";
    $declaracao .= '<table border="1" style="border-collapse: collapse;" cellpadding="2">
                        <thead>
                            <tr>
                                <th>Candidato (Tipo)</th>
                                <th>Campus</th>
                                <th>Quantidade de votos nesta seção</th>
                            </tr>
                        </thead>
                        <tbody>';
    if (isset($candidatos)) {
        $total_votos_em_candidatos = 0;
        foreach ($candidatos as $candidato) {
            $qt_votos = (empty($votos_por_candidato[$candidato->id]) ? 0 : $votos_por_candidato[$candidato->id]);
            $declaracao .= '<tr>' .
                            "<td>$candidato->nome (" . $candidato->tipo->nome . ")</td>" .
                            "<td>" . $candidato->campus->nome . "</td>" .
                            "<td>$qt_votos</td>" .
                        '</tr>';
            $total_votos_em_candidatos += $qt_votos;
        }
        $declaracao .= '<tr>' .
                            "<td colspan='2'><b>Subtotal</b></td>" .
                            "<td><b>$total_votos_em_candidatos</b></td>" .
                        '</tr>' .
                        '<tr>' .
                            "<td colspan='2'>Votos em branco</td>" .
                            "<td>" . (empty($votos_brancos_nulos['branco']) ? '0' : $votos_brancos_nulos['branco']) . "</td>" .
                        '</tr>' .
                        '<tr>' .
                            "<td colspan='2'>Votos nulos</td>" .
                            "<td>" . (empty($votos_brancos_nulos['nulo']) ? '0' : $votos_brancos_nulos['nulo']) . "</td>" .
                        '</tr>' .
                        '<tr>' .
                            "<td colspan='2'><b>TOTAL</b></td>" .
                            "<td><b>$total_votos</b></td>" .
                        '</tr>';
    }
    $declaracao .= '<tbody></table>';

    ?>
    <div class="secoes form large-9 medium-8 columns content">
        <?= $this->Form->create($secao) ?>
        <fieldset>
            <legend>Abrir Seção</legend>
            <?php
                echo $this->Form->hidden('id');
                echo $this->Form->hidden('declaracao', ['value' => $declaracao]);
                echo $declaracao;
            ?>
        </fieldset>
        <?= $this->Form->button('Abrir', ['class' => 'button small round success']) ?>
        <?= $this->Form->end() ?>
    </div>
<?
}
?>