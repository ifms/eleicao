<div class="secoes index large-9 medium-8 columns content">
    <?= $this->Html->link('Adicionar', ['action' => 'add'], ['class' => 'button success round tiny']) ?>
    <h3><?= __('Seções') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th><?= $this->Paginator->sort('campus_id') ?></th>
                <th><?= $this->Paginator->sort('abertura_autorizada') ?></th>
                <th><?= $this->Paginator->sort('fechamento_autorizado') ?></th>
                <th><?= $this->Paginator->sort('abertura') ?></th>
                <th><?= $this->Paginator->sort('fechamento') ?></th>
                <th><?= $this->Paginator->sort('tipo') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($secoes as $secao): ?>
            <tr>
                <td><?= $this->Number->format($secao->id) ?></td>
                <td><?= h($secao->nome) ?></td>
                <td><?= $secao->campus->nome ?></td>
                <td><?= $secao->abertura_autorizada ?></td>
                <td><?= $secao->fechamento_autorizado ?></td>
                <td><?= $secao->abertura ?></td>
                <td><?= $secao->fechamento ?></td>
                <td><?= $secao->tipo ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $secao->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $secao->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $secao->id], ['confirm' => __('Are you sure you want to delete # {0}?', $secao->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
