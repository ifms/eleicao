<div class="secoes form large-9 medium-8 columns content">
    <?= $this->Form->create($secao) ?>
    <fieldset>
        <legend><?= __('Edit Secao') ?></legend>
        <?php
            echo $this->Form->input('nome');
            echo $this->Form->input('campus_id', ['options' => $campi]);
            echo $this->Form->input('abertura', ['type' => 'text']);
            echo $this->Form->input('fechamento', ['type' => 'text']);
            echo $this->Form->input('abertura_autorizada', ['type' => 'text']);
            echo $this->Form->input('fechamento_autorizado', ['type' => 'text']);
            echo $this->Form->input('tipo', ['options' => ['sistema' => 'Votação via sistema', 'manual' => 'Votação manual', 'autenticada' => 'Votação usando SIAPE e senha SUAP']]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
