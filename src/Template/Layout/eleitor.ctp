<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

use App\Model\Entity\Configuracao;
$configuracoes = new Configuracao();
$title = $configuracoes->le('titulo_site');
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $title ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('cake.css?' . time()) ?>

    <?= $this->Html->script(['jquery-1.11.3.min','jquery.blockUI.min','jquery.maskedinput.min']) ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
    <nav class="top-bar expanded" data-topbar role="navigation">
        <ul class="title-area large-3 medium-4 small-12 columns">
            <li class="name">
                <h1><?= $this->Html->link($title, '/');?></h1>
            </li>
        </ul>
        <section class="top-bar-section show-for-medium-up large-9 medium-8 columns">
            <ul class="right">
                <?php
                // Recupero usuário logado, caso haja
                $user = $this->request->session()->read('Auth.User');

                if (!$user) {?>
                    <li><a href="">Instituto Federal de Mato Grosso do Sul</a></li>
                <?php
                }else{ ?>
                    <li><a href=""><?php echo $user['nome'] . '</a></li><li>' . $this->Html->link('Sair', ['controller' => 'usuarios', 'action' => 'logout']);?></li>
                <?php
                } ?>
            </ul>
        </section>
    </nav>
    <?= $this->Flash->render() ?>
    <section class="container clearfix">
        <?= $this->fetch('content') ?>
    </section>
    <footer style="background: #116d76; left: 0; bottom: 0; width: 100%; text-align: center;">
        <a style="color: #FFF; font-size: 0.8em" href="mailto:<?php echo $configuracoes->le('email_comissao'); ?>"><strong>Contato com a organização da eleição: </strong><?php echo $configuracoes->le('email_comissao'); ?></a>
    </footer>
</body>
</html>
