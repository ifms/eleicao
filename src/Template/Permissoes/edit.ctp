<div class="permissoes form large-9 medium-8 columns content">
    <?= $this->Form->create($permissao) ?>
    <fieldset>
        <legend><?= __('Edit Permissao') ?></legend>
        <?php
            echo $this->Form->input('grupo_id', ['options' => $grupos]);
            echo $this->Form->input('controller');
            echo $this->Form->input('action');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
