<div class="permissoes view large-9 medium-8 columns content">
    <h3><?= h($permissao->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Grupo') ?></th>
            <td><?= $permissao->has('grupo') ? $this->Html->link($permissao->grupo->nome, ['controller' => 'Grupos', 'action' => 'view', $permissao->grupo->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Controller') ?></th>
            <td><?= h($permissao->controller) ?></td>
        </tr>
        <tr>
            <th><?= __('Action') ?></th>
            <td><?= h($permissao->action) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($permissao->id) ?></td>
        </tr>
    </table>
</div>
