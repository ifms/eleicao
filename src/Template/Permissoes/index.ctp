<div class="permissoes index large-9 medium-8 columns content">
    <?= $this->Html->link('Adicionar', ['action' => 'add'], ['class' => 'button success round tiny']) ?>
    <h3><?= __('Permissoes') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('grupo_id') ?></th>
                <th><?= $this->Paginator->sort('controller') ?></th>
                <th><?= $this->Paginator->sort('action') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($permissoes as $permissao): ?>
            <tr>
                <td><?= $this->Number->format($permissao->id) ?></td>
                <td><?= $permissao->has('grupo') ? $this->Html->link($permissao->grupo->nome, ['controller' => 'Grupos', 'action' => 'view', $permissao->grupo->id]) : '' ?></td>
                <td><?= h($permissao->controller) ?></td>
                <td><?= h($permissao->action) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $permissao->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $permissao->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $permissao->id], ['confirm' => __('Are you sure you want to delete # {0}?', $permissao->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
