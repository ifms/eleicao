<div class="auditorias view large-9 medium-8 columns content">
    <h3><?= h($auditoria->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Acao') ?></th>
            <td><?= h($auditoria->acao) ?></td>
        </tr>
        <tr>
            <th><?= __('Usuario') ?></th>
            <td><?= $auditoria->has('usuario') ? $this->Html->link($auditoria->usuario->id, ['controller' => 'Usuarios', 'action' => 'view', $auditoria->usuario->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Secao') ?></th>
            <td><?= $auditoria->has('secao') ? $this->Html->link($auditoria->secao->id, ['controller' => 'Secoes', 'action' => 'view', $auditoria->secao->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($auditoria->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Registro') ?></th>
            <td><?= h($auditoria->registro) ?></td>
        </tr>
    </table>
</div>
