<div class="auditorias index large-9 medium-8 columns content">
    <?= $this->Html->link('Adicionar', ['action' => 'add'], ['class' => 'button success round tiny']) ?>
    <h3><?= __('Auditorias') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th style="width: 40px"><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('acao') ?></th>
                <th><?= $this->Paginator->sort('registro') ?></th>
                <th><?= $this->Paginator->sort('usuario_id') ?></th>
                <th><?= $this->Paginator->sort('secao_id') ?></th>
                <th style="width: 140px" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($auditorias as $auditoria): ?>
            <tr>
                <td><?= $this->Number->format($auditoria->id) ?></td>
                <td><?= h($auditoria->acao) ?></td>
                <td><?= $auditoria->registro->format('d/m/Y à\s H:i:s') ?></td>
                <td><?= $auditoria->has('usuario') ? $this->Html->link($auditoria->usuario->nome . ' (' . $auditoria->usuario->grupo->nome . ')', ['controller' => 'Usuarios', 'action' => 'view', $auditoria->usuario->id]) : '' ?></td>
                <td><?= $auditoria->has('secao') ? $this->Html->link($auditoria->secao->id . ' - ' . $auditoria->secao->nome, ['controller' => 'Secoes', 'action' => 'view', $auditoria->secao->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $auditoria->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $auditoria->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $auditoria->id], ['confirm' => __('Are you sure you want to delete # {0}?', $auditoria->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
