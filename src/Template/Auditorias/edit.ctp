<div class="auditorias form large-9 medium-8 columns content">
    <?= $this->Form->create($auditoria) ?>
    <fieldset>
        <legend><?= __('Edit Auditoria') ?></legend>
        <?php
            echo $this->Form->input('acao');
            echo $this->Form->input('registro');
            echo $this->Form->input('usuario_id', ['options' => $usuarios]);
            echo $this->Form->input('secao_id', ['options' => $secoes]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
