<div class="usuarios form">
<?= $this->Flash->render('auth') ?>
	<div>
		<div class="small-12 medium-5 large-3 end columns">
			<fieldset>
				<legend><?= __('Área administrativa') ?></legend>
				<p><?= __('Por favor entre com sua matrícula SIAPE e sua senha do SUAP') ?></p>
				<?= $this->Form->create() ?>
				<?= $this->Form->input('siape', ['label' => 'SIAPE']); ?>
				<?= $this->Form->input('senha', ['type' => 'password']) ?>
				<?= $this->Form->button(__('Entrar usando SUAP'), ['class' => 'button small success round', 'style' => 'margin-left: 0']); ?>
				<?= $this->Form->end() ?>
			</fieldset>
			<fieldset>
			<? echo 'Login antigo: ' . $this->Html->link('entre usando CPF e senha', ['#'], ['class' => '', 'id' => 'mostrar_login_normal']); ?>
			</fieldset>
			<fieldset id="login_normal" style="display: none">
				<legend><?= __('Área administrativa') ?></legend>
				<p><?= __('Por favor entre com seu CPF e senha') ?></p>
				<?= $this->Form->create('Usuarios', ['url' => ['controller' => 'Usuarios', 'action' => 'login']]) ?>
				<?= $this->Form->input('cpf', ['label' => 'CPF', 'maxlength' => 11]); ?>
				<?= $this->Form->input('senha', ['type' => 'password']) ?>
				<?= $this->Form->button(__('Entrar'), ['class' => 'button small info round', 'style' => 'margin-left: 0']); ?>
				<?= $this->Form->end() ?>
			</fieldset>
		</div>
	</div>
</div>
<script>
$(document).ready(function() {
    $('#mostrar_login_normal').click(function(e){
		e.preventDefault();
		$('#login_normal').toggle();
	});
});
</script>
