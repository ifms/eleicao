<div class="usuarios form large-9 medium-8 columns content">
    <?= $this->Form->create($usuario) ?>
    <fieldset>
        <legend><?= __('Edit Usuario') ?></legend>
        <?php
            echo $this->Form->input('cpf');
            echo $this->Form->input('nome');
            echo $this->Form->input('senha', ['type' => 'password']);
            echo $this->Form->input('tipo_id', ['options' => $tipos]);
            echo $this->Form->input('grupo_id', ['options' => $grupos]);
            echo $this->Form->input('secao_id', ['options' => $secoes]);
            echo $this->Form->input('secao_trabalho_id', ['options' => $secoes, 'empty' => ' - SELECIONE - ']);
            echo $this->Form->input('status_id', ['options' => $status]);
            echo $this->Form->input('email');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
