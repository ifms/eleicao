<div class="usuarios form large-9 medium-8 columns content">
    <?= $this->Form->create($usuario) ?>
    <fieldset>
        <legend>Liberar eleitor</legend>
        <div class="help">Entre com o nome do eleitor ou com o CPF do mesmo e clique em buscar para prosseguir com a liberação. Caso não obtenha resultados experimente entrar com parte do nome do eleitor ou parte do seu CPF. Ao encontrar o eleitor esperado, confirme os dados (nome, CPF e Seção) e então clique em "Liberar para votação":</div>
        <?
            echo $this->Form->input('busca', ['label' => 'Buscar por parte do nome ou CPF']);
        ?>
    </fieldset>
    <?= $this->Form->button('Buscar', ['class' => 'warning round small']) ?>
    <?= $this->Form->end() ?>
</div>
<div class="usuarios form large-9 medium-8 columns content">    
    <?
    if (isset($eleitor)) { ?>
        <fieldset>
            <legend>Resultados da busca (<?= count($eleitor);?>)</legend>
            <?
            if (empty($eleitor)) {
                echo '<p><strong>Nenhum eleitor encontrado com os critérios informados. Tente novamente.</strong></p>';
            }
            else {?>
                <table>
                    <thead>
                        <tr>
                            <th>Nome (Categoria)</th>
                            <th>CPF</th>
                            <th>Seção</th>
                            <th>Situação</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?
                        foreach ($eleitor as $eleitor) { ?>
                            <tr>
                                <td <?php echo in_array($eleitor->tipo->id, array(9,4)) ? 'style="color:blue;"' : ''; ?>><?= $eleitor->nome . ' (' . $eleitor->tipo->nome . ')' ?></td>
                                <td <?php echo in_array($eleitor->tipo->id, array(9,4)) ? 'style="color:blue;"' : ''; ?>><?= $eleitor->cpf ?></td>
                                <td><?= $eleitor->secao->id . ' - ' . $eleitor->secao->nome ?></td>
                                <td><?= $eleitor->status->nome ?></td>
                                <td><?
                                switch($eleitor->status->id){
                                    case 1:  // AGUARDANDO LIBERAÇÃO
                                        if ($user['secao_trabalho_id'] == $eleitor->secao_id) {
                                            echo $this->Form->postLink('Liberar para votação',[  
                                                    'controller' => 'Usuarios', 
                                                    'action' => 'liberar_eleitor'
                                                ],[
                                                    'confirm' => 'Deseja realmente liberar o eleitor ' . $eleitor->nome . ' (CPF: ' . $eleitor->cpf . ') para votação?',
                                                    'data' => [
                                                        'eleitor_id' => $eleitor->id,
                                                        'busca' => $eleitor->cpf,
                                                        'acao' => 'liberar_eleitor'
                                                    ],
                                                    'class' => ['button small round success'],
                                                    'style' => ['margin: 5px 0']
                                                ]
                                            ); 
                                        } else
                                            echo 'Eleitor de outra Seção';
                                        break;
                                    case 2:  // LIBERADO PARA VOTAR
                                        echo 'Já liberado';
                                        if ($user['secao_trabalho_id'] == $eleitor->secao_id) {
                                            echo $this->Form->postLink('Desfazer liberação',[  
                                                        'controller' => 'Usuarios', 
                                                        'action' => 'liberar_eleitor'
                                                    ],[
                                                        'confirm' => 'Deseja realmente retornar o eleitor ' . $eleitor->nome . ' (CPF: ' . $eleitor->cpf . ') para a situação "AGUARDANDO LIBERAÇÃO"?',
                                                        'data' => [
                                                            'eleitor_id' => $eleitor->id,
                                                            'busca' => $eleitor->cpf,
                                                            'acao' => 'desfazer_liberacao'
                                                        ],
                                                        'class' => ['button tiny round alert'],
                                                        'style' => ['margin: 5px 0 5px 20px']
                                                    ]
                                                ); 
                                        }
                                        break;
                                    case 3:  // JÁ VOTOU
                                        echo 'Já votou';
                                        break;
                                    case 4:  // BLOQUEADO
                                        echo 'Impedido de votar';
                                        break;
                                    default:  // 
                                        echo 'Entrar em contato com gestor';
                                        break;    
                                }
                                ?></td>
                            </tr>
                        <?    
                        }
                        ?>
                    </tbody>
                </table>
            <?
            }?>
        </fieldset>
        <?
    }
    ?>
</div>
