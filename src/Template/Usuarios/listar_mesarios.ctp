<div class="usuarios form large-9 medium-8 columns content">    
    <?
    if (isset($eleitor)) { ?>
        <fieldset>
            <legend>Lista de Mesários (<?= count($eleitor);?>)</legend>
            <?
            if (empty($eleitor)) {
                echo '<p><strong>Nenhum mesário encontrado.</strong></p>';
            }
            else {?>
                <table>
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>CPF</th>
                            <th>Seção</th>
                            
                            <th colspan="2"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?
                        foreach ($eleitor as $eleitor) { ?>
                            <tr>
                                <td><?= $eleitor->nome ?></td>
                                <td><?= $eleitor->cpf ?></td>
                                <td><?= $eleitor->secao->id . ' - ' . $eleitor->secao->nome ?></td>
                                <td><?= $this->Html->link('editar', ['controller' => 'usuarios', 'action' => 'edit', $eleitor->id], ['class' => ['button small round success'], 'style' => ['margin: 5px 0']]) ?></td>
                                <td><?= $this->Html->link('Resetar Senha', ['controller' => 'usuarios', 'action' => 'resetSenha', $eleitor->id], ['class' => ['button small round warning'], 'style' => ['margin: 5px 0']]) ?></td>
                            </tr>
                        <?    
                        }
                        ?>
                    </tbody>
                </table>
            <?
            }?>
        </fieldset>
        <?
    }
    ?>
</div>
