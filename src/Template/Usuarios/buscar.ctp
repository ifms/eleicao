<div class="usuarios form large-9 medium-8 columns content">
    <?= $this->Form->create($usuario) ?>
    <fieldset>
        <legend>Buscar usuário</legend>
        <div class="help">Entre com o nome do usuário ou com o CPF do mesmo e clique em buscar para prosseguir:</div>
        <?
            echo $this->Form->input('busca', ['label' => 'Buscar por parte do nome ou CPF']);
        ?>
    </fieldset>
    <?= $this->Form->button('Buscar', ['class' => 'warning round small']) ?>
    <?= $this->Form->end() ?>
</div>
<div class="usuarios form large-9 medium-8 columns content">    
    <?
    if (isset($eleitor)) { ?>
        <fieldset>
            <legend>Resultados da busca (<?= count($eleitor);?>)</legend>
            <?
            if (empty($eleitor)) {
                echo '<p><strong>Nenhum eleitor encontrado com os critérios informados. Tente novamente.</strong></p>';
            }
            else {?>
                <table>
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>CPF</th>
                            <th>Seção</th>
                            <th>Situação</th>
                            <th colspan="2"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?
                        foreach ($eleitor as $eleitor) { ?>
                            <tr>
                                <td><?= $eleitor->nome ?></td>
                                <td><?= $eleitor->cpf ?></td>
                                <td><?= $eleitor->secao->id . ' - ' . $eleitor->secao->nome ?></td>
                                <td><?= $eleitor->status->nome ?></td>
                                <td><?= $this->Html->link('editar', ['controller' => 'usuarios', 'action' => 'edit', $eleitor->id], ['class' => ['button small round success'], 'style' => ['margin: 5px 0']]) ?></td>
                                <td><?= $this->Html->link('Resetar Senha', ['controller' => 'usuarios', 'action' => 'resetSenha', $eleitor->id], ['class' => ['button small round warning'], 'style' => ['margin: 5px 0']]) ?></td>
                            </tr>
                        <?    
                        }
                        ?>
                    </tbody>
                </table>
            <?
            }?>
        </fieldset>
        <?
    }
    ?>
</div>
