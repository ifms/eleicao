<div class="usuarios view large-9 medium-8 columns content">
    <h3><?= h($usuario->nome) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Cpf') ?></th>
            <td><?= h($usuario->cpf) ?></td>
        </tr>
        <tr>
            <th><?= __('Tipo') ?></th>
            <td><?= $usuario->has('tipo') ? $this->Html->link($usuario->tipo->nome, ['controller' => 'Tipos', 'action' => 'view', $usuario->tipo->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Grupo') ?></th>
            <td><?= $usuario->has('grupo') ? $this->Html->link($usuario->grupo->nome, ['controller' => 'Grupos', 'action' => 'view', $usuario->grupo->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Secao') ?></th>
            <td><?= $usuario->has('secao') ? $this->Html->link($usuario->secao->nome, ['controller' => 'Secoes', 'action' => 'view', $usuario->secao->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Status') ?></th>
            <td><?= $usuario->has('status') ? $this->Html->link($usuario->status->nome, ['controller' => 'Status', 'action' => 'view', $usuario->status->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($usuario->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4>Registro de atividades:</h4>
        <?php if (!empty($usuario->auditorias)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th style="width: 40px"><?= __('Id') ?></th>
                <th><?= __('Ação') ?></th>
                <th><?= __('Registro') ?></th>
                <th style="width: 140px" class="actions"><?= __('Ações') ?></th>
            </tr>
            <?php foreach ($usuario->auditorias as $auditorias): ?>
            <tr>
                <td><?= h($auditorias->id) ?></td>
                <td><?= h($auditorias->acao) ?></td>
                <td><?= h($auditorias->registro->format('d/m/Y à\s H:i:s')) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Auditorias', 'action' => 'view', $auditorias->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Auditorias', 'action' => 'edit', $auditorias->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Auditorias', 'action' => 'delete', $auditorias->id], ['confirm' => __('Are you sure you want to delete # {0}?', $auditorias->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
