<div class="usuarios index large-9 medium-8 columns content">
    <?= $this->Html->link('Adicionar', ['action' => 'add'], ['class' => 'button success round tiny']) ?>
    <h3><?= __('Usuarios') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('cpf') ?></th>
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th><?= $this->Paginator->sort('tipo_id') ?></th>
                <th><?= $this->Paginator->sort('grupo_id') ?></th>
                <th><?= $this->Paginator->sort('secao_id') ?></th>
                <th><?= $this->Paginator->sort('secao_trabalho_id', ['label' => 'Seção de trabalho']) ?></th>
                <th><?= $this->Paginator->sort('status_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($usuarios as $usuario): ?>
            <tr>
                <td><?= $this->Number->format($usuario->id) ?></td>
                <td><?= h($usuario->cpf) ?></td>
                <td><?= h($usuario->nome) ?></td>
                <td><?= $usuario->has('tipo') ? $this->Html->link($usuario->tipo->nome, ['controller' => 'Tipos', 'action' => 'view', $usuario->tipo->id]) : '' ?></td>
                <td><?= $usuario->has('grupo') ? $this->Html->link($usuario->grupo->nome, ['controller' => 'Grupos', 'action' => 'view', $usuario->grupo->id]) : '' ?></td>
                <td><?= $usuario->has('secao') ? $this->Html->link($usuario->secao->nome, ['controller' => 'Secoes', 'action' => 'view', $usuario->secao->id]) : '' ?></td>
                <td><?= $usuario->has('secao_trabalho') ? $this->Html->link($usuario->secao_trabalho->nome, ['controller' => 'Secoes', 'action' => 'view', $usuario->secao_trabalho->id]) : '' ?></td>
                <td><?= $usuario->has('status') ? $this->Html->link($usuario->status->nome, ['controller' => 'Status', 'action' => 'view', $usuario->status->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $usuario->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $usuario->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $usuario->id], ['confirm' => __('Are you sure you want to delete # {0}?', $usuario->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
