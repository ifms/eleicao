<div class="usuarios form large-9 medium-8 columns content">
    <?= $this->Form->create('Usuario') ?>
    <fieldset>
        <legend>Lista de eleitores por Seção</legend>
        <div class="large-3">
            <?php            
                echo $this->Form->input('secao_id', ['options' => $secoes->toArray() + ['todas' => 'TODAS'], 'label' => 'Seção', 'empty' => 'SELECIONE']);
            ?>
        </div>
    </fieldset>
    <?= $this->Form->button('Buscar', ['class' => 'success small round']) ?>
    <?= $this->Form->end() ?>
</div>
<div class="usuarios form large-9 medium-8 columns content">
    <? if (isset($eleitores)): 
        // Caixas com contagem por STATUS
        echo '<ul class="button-group radius even-' . $status->count() . '">';
        foreach ($status as $status_id => $status_label)
        {
            echo '<li><a href="#" class="button secondary">' . $status_label . '<br><span style="font-weight: bold; font-size: 150%">' . (isset($contagem[$status_id]) ? $contagem[$status_id] : '0') . '</span></a></li>';
        }
        echo '</ul>';
        $total_eleitores = $eleitores->count(); ?>
        <fieldset>
            <legend>Resultados da busca (<?= $total_eleitores ?>):</legend>
        <?  
            if ($total_eleitores > 0): ?>
            <table>
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>CPF</th>
                        <th>Tipo</th>
                        <th>Seção</th>
                        <th>Situação</th>
                    </tr>
                </thead>
                <tbody>
                <? foreach ($eleitores as $eleitor): ?>
                    <tr>
                        <td><?= $eleitor->nome ?></td>
                        <td><?= $eleitor->cpf ?></td>
                        <td><?= $eleitor->tipo->nome ?></td>
                        <td><?= $eleitor->secao->id . ' - ' . $eleitor->secao->nome ?></td>
                        <td><?= $eleitor->status->nome ?></td>
                    </tr>
                <? endforeach; ?>    
                </tbody>
            </table>
        <? else: ?>
            <p>Nenhum eleitor cadastrado nesta Seção.</p>
        <? endif; ?>
        </fieldset>
    <? endif; ?>
</div>
