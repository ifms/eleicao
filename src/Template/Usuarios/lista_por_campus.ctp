<div class="usuarios form large-9 medium-8 columns content">
    <?= $this->Form->create('Usuario') ?>
    <fieldset>
        <legend>Lista de eleitores por Campus</legend>
        <div class="large-3">
            <?php            
                echo $this->Form->input('campus_id', ['options' => $campi->toArray() + ['todos' => 'TODOS'], 'label' => 'Campus', 'empty' => 'SELECIONE']);
            ?>
        </div>
    </fieldset>
    <?= $this->Form->button('Buscar', ['class' => 'success small round']) ?>
    <?= $this->Form->end() ?>
</div>
<div class="usuarios form large-9 medium-8 columns content">
    <? if (isset($eleitores)): 
        $total_eleitores = $eleitores->count(); ?>
        <fieldset>
            <legend>Resultados da busca (<?= $total_eleitores ?>):</legend>
        <?  
            if ($total_eleitores > 0): ?>
            <table>
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>CPF</th>
                        <th>Tipo</th>
                        <th>Campus</th>
                        <th>Seção</th>
                        <th>Situação</th>
                    </tr>
                </thead>
                <tbody>
                <? foreach ($eleitores as $eleitor): ?>
                    <tr>
                        <td><?= $eleitor->nome ?></td>
                        <td><?= $eleitor->cpf ?></td>
                        <td><?= $eleitor->tipo->nome ?></td>
                        <td><?= $eleitor->secao->campus->nome ?></td>
                        <td><?= $eleitor->secao->id . ' - ' . $eleitor->secao->nome ?></td>
                        <td><?= $eleitor->status->nome ?></td>
                    </tr>
                <? endforeach; ?>    
                </tbody>
            </table>
        <? else: ?>
            <p>Nenhum eleitor cadastrado neste Campus.</p>
        <? endif; ?>
        </fieldset>
    <? endif; ?>
</div>
