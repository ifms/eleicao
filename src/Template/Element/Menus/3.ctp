<? /* MENU GESTOR */ ?>
<ul class="side-nav">
    <li class="heading">Menu</li>
    <li><?= $this->Html->link('Candidatos', ['controller' => 'Candidatos', 'action' => 'index']) ?></li>
    <li><?= $this->Html->link('Tipos de usuários', ['controller' => 'Tipos', 'action' => 'index']) ?></li>
    <li><?= $this->Html->link('Seções', ['controller' => 'Secoes', 'action' => 'index']) ?></li>
    <li><?= $this->Html->link('Campi', ['controller' => 'Campi', 'action' => 'index']) ?></li>
    <li class="heading">Relatórios</li>
    <li><?= $this->Html->link('Lista de eleitores por Seção', ['controller' => 'usuarios', 'action' => 'listaPorSecao']) ?></li>
    <li><?= $this->Html->link('Contagem de votos', ['controller' => 'Votos', 'action' => 'contagemVotos'], ['target' => '_blank']) ?></li>
</ul>