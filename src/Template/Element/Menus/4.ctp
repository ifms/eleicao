<? /* MENU ADMINISTRADOR */ ?>
<ul class="side-nav">
    <li class="heading">Menu</li>
    <li><?= $this->Html->link('Buscar Usuário', ['controller' => 'Usuarios', 'action' => 'buscar']) ?></li>
    <li><?= $this->Html->link('Auditorias', ['controller' => 'Auditorias', 'action' => 'index']) ?></li>
    <li><?= $this->Html->link('Campi', ['controller' => 'Campi', 'action' => 'index']) ?></li>
    <li><?= $this->Html->link('Candidatos', ['controller' => 'Candidatos', 'action' => 'index']) ?></li>
    <li><?= $this->Html->link('Grupos', ['controller' => 'Grupos', 'action' => 'index']) ?></li>
    <li><?= $this->Html->link('Permissões', ['controller' => 'Permissoes', 'action' => 'index']) ?></li>
    <li><?= $this->Html->link('Seções', ['controller' => 'Secoes', 'action' => 'index']) ?></li>
    <li><?= $this->Html->link('Situações de usuários', ['controller' => 'Status', 'action' => 'index']) ?></li>
    <li><?= $this->Html->link('Tipos de usuários', ['controller' => 'Tipos', 'action' => 'index']) ?></li>
    <li><?= $this->Html->link('Usuários', ['controller' => 'Usuarios', 'action' => 'index']) ?></li>
    <li><?= $this->Html->link('Votos', ['controller' => 'Votos', 'action' => 'index']) ?></li>
    <li class="heading">Relatórios</li>
    <li><?= $this->Html->link('Lista de eleitores por Seção', ['controller' => 'Usuarios', 'action' => 'listaPorSecao']) ?></li>
    <li><?= $this->Html->link('Lista de eleitores por Campus', ['controller' => 'Usuarios', 'action' => 'listaPorCampus']) ?></li>
    <li><?= $this->Html->link('Lista Mesários', ['controller' => 'Usuarios', 'action' => 'listarMesarios']) ?></li>
    <li><?= $this->Html->link('Contagem de votos', ['controller' => 'Votos', 'action' => 'contagemVotos'], ['target' => '_blank']) ?></li>
    <li><?= $this->Html->link('Lista códigos de votação', ['controller' => 'Votos', 'action' => 'listaCodigos'], ['target' => '_blank']) ?></li>
    <li class="heading">Outros</li>
    <li><?= $this->Html->link('Configurações', ['controller' => 'Configuracoes', 'action' => 'index']) ?></li>
    <li><?= $this->Html->link('Teste de Som', ['controller' => 'Votos', 'action' => 'testeSom']) ?></li>
</ul>