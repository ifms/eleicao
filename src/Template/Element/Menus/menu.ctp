<ul class="side-nav">
    <li class="heading"><?= __('Actions') ?></li>
    <li><?= $this->Html->link(__('Edit Usuario'), ['action' => 'edit', $usuario->id]) ?> </li>
    <li><?= $this->Form->postLink(__('Delete Usuario'), ['action' => 'delete', $usuario->id], ['confirm' => __('Are you sure you want to delete # {0}?', $usuario->id)]) ?> </li>
    <li><?= $this->Html->link(__('List Usuarios'), ['action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Usuario'), ['action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Tipos'), ['controller' => 'Tipos', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Tipo'), ['controller' => 'Tipos', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Grupos'), ['controller' => 'Grupos', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Grupo'), ['controller' => 'Grupos', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Secoes'), ['controller' => 'Secoes', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Secao'), ['controller' => 'Secoes', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Status'), ['controller' => 'Status', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Status'), ['controller' => 'Status', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Auditorias'), ['controller' => 'Auditorias', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Auditoria'), ['controller' => 'Auditorias', 'action' => 'add']) ?> </li>
</ul>