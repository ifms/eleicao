<? /* MENU MESÁRIO */ ?>
<ul class="side-nav">
    <li class="heading">Menu</li>
    <li><?= $this->Html->link('Liberar Eleitor', ['controller' => 'Usuarios', 'action' => 'liberar_eleitor']) ?></li>
    <li><?= $this->Html->link('Candidatos', ['controller' => 'Candidatos', 'action' => 'index']) ?></li>
    <li><?= $this->Html->link('Tipos de usuários', ['controller' => 'Tipos', 'action' => 'index']) ?></li>
    <li><?= $this->Html->link('Seções', ['controller' => 'Secoes', 'action' => 'index']) ?></li>
    <li><?= $this->Html->link('Campi', ['controller' => 'Campi', 'action' => 'index']) ?></li>
    <li><?= $this->Html->link('Ajuda', 'https://docs.google.com/a/ifms.edu.br/presentation/d/1VxrD_ZIwnH54UQs9UCr8DuEK6DC16ziYOQJXnm5fCX8/edit?usp=sharing', ['target' => '_blank']) ?></li>
    <li class="heading">Relatórios</li>
    <li><?= $this->Html->link('Lista de eleitores por Seção', ['controller' => 'usuarios', 'action' => 'listaPorSecao']) ?></li>
    <li class="heading">Ações da Seção</li>
    <li><?= $this->Html->link('Abrir Seção', ['controller' => 'secoes', 'action' => 'abrir']) ?></li>
    <li><?= $this->Html->link('Fechar Seção', ['controller' => 'secoes', 'action' => 'fechar']) ?></li>
</ul>