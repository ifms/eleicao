<div class="configuracoes index large-9 medium-8 columns content">
    <?= $this->Html->link('Adicionar', ['action' => 'add'], ['class' => 'button success round tiny']) ?>
    <h3><?= __('Configurações') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('chave') ?></th>
                <th><?= $this->Paginator->sort('valor') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($configuracoes as $configuracao): ?>
            <tr>
                <td><?= $this->Number->format($configuracao->id) ?></td>
                <td><?= h($configuracao->chave) ?></td>
                <td><?= h($configuracao->valor) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $configuracao->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $configuracao->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $configuracao->id], ['confirm' => __('Are you sure you want to delete # {0}?', $configuracao->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
