<div class="configuracoes view large-9 medium-8 columns content">
    <h3><?= h($configuracao->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Chave') ?></th>
            <td><?= h($configuracao->chave) ?></td>
        </tr>
        <tr>
            <th><?= __('Valor') ?></th>
            <td><?= h($configuracao->valor) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($configuracao->id) ?></td>
        </tr>
    </table>
</div>
