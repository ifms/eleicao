<div class="configuracoes form large-9 medium-8 columns content">
    <?= $this->Form->create($configuracao) ?>
    <fieldset>
        <legend><?= __('Add Configuracao') ?></legend>
        <?php
            echo $this->Form->input('chave');
            echo $this->Form->input('valor');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
