<?php
namespace App\Model\Table;

use App\Model\Entity\Auditoria;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Auditorias Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Usuarios
 * @property \Cake\ORM\Association\BelongsTo $Secoes
 */
class AuditoriasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('auditorias');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Usuarios', [
            'foreignKey' => 'usuario_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Secoes', [
            'foreignKey' => 'secao_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('acao', 'create')
            ->notEmpty('acao');

        $validator
            ->requirePresence('registro', 'create')
            ->notEmpty('registro');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['usuario_id'], 'Usuarios'));
        $rules->add($rules->existsIn(['secao_id'], 'Secoes'));
        return $rules;
    }

    /*
     * Gravar uma auditoria/log
     */
    public function add($dados = null)
    {
        if ($dados)
        {
            $auditoria = $this->newEntity();
            $dados['registro'] = date('Y-m-d H:i:s'); // guardo a data e hora atual
            $this->patchEntity($auditoria, $dados);
            if ($this->save($auditoria))
                return true;
        }
        return false;
    }
}
