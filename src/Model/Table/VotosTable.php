<?php
namespace App\Model\Table;

use App\Model\Entity\Voto;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
/**
 * Votos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Candidatos
 * @property \Cake\ORM\Association\BelongsTo $Secoes
 */
class VotosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('votos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Candidatos', [
            'foreignKey' => 'candidato_id'
        ]);
        $this->belongsTo('Secoes', [
            'foreignKey' => 'secao_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('registro', 'create')
            ->notEmpty('registro');

        $validator
            ->add('branco', 'valid', ['rule' => 'boolean'])
            ->allowEmpty('branco');

        $validator
            ->add('nulo', 'valid', ['rule' => 'boolean'])
            ->allowEmpty('nulo');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['candidato_id'], 'Candidatos'));
        $rules->add($rules->existsIn(['secao_id'], 'Secoes'));
        return $rules;
    }

    /*
     * Gera todos os códigos numéricos de tamanho $tamanho randômico na tabela códigos
     */

    public function gera_todos_codigos ($tamanho = 4) {

        $conn = ConnectionManager::get('default');

        for ($i = 0; $i < 1000; $i++) { 
            // Gero um número que ainda não foi salvo no banco
            $salvou_codigo = false;
            do {
                $codigo = "";

                for ($j = 0; $j < $tamanho; $j++) {
                    $codigo .= rand(0, 9);
                }
                // Verifico se já existe código
                $ja_existe_codigo = $conn->execute('SELECT count(*) FROM codigos WHERE codigo = ?;', [$codigo]
                )->fetch('assoc');

                // Se não existe o código ainda
                if ($ja_existe_codigo['count'] == 0)
                {
                    $conn->execute('INSERT INTO codigos(codigo, dv) VALUES (?, ?);', [$codigo, $this->gera_dv($codigo)]
                    );
                    $salvou_codigo = true;
                }
                else {
                    debug('Conflito: ' . $codigo);
                    $salvou_codigo = false;
                }
            } while (!$salvou_codigo);
        }
    }

    /**
     * Calculate check digit according to Luhn's algorithm
     * New method (suggested by H.Johnson), see http://www.phpclasses.org/discuss/package/8471/thread/1/
     */
    public function gera_dv ($codigo) {
        $sumTable = array(array(0,1,2,3,4,5,6,7,8,9),array(0,2,4,6,8,1,3,5,7,9));
    
        $length = strlen($codigo);
        $sum = 0;
        $flip = 1;
        // Sum digits (last one is check digit, which is not in parameter)
        for($i=$length-1;$i>=0;--$i) $sum += $sumTable[$flip++ & 0x1][$codigo[$i]];
        // Multiply by 9
        $sum *= 9;
        // Last digit of sum is check digit
        return (int)substr($sum,-1,1);
    }

    /**
     * Reserva código
     */
    public function reserva_codigo () {
        $conn = ConnectionManager::get('default');
        $conn->begin();
        // Seleciono uma linha do código randomicamente para obter o código e travo ela para o UPDATE
        $codigo = $conn->execute('SELECT * FROM codigos WHERE livre = ? ORDER BY random() LIMIT 1 FOR UPDATE;', [true])->fetch('assoc');
        if (!empty($codigo))
            $conn->execute('UPDATE codigos SET livre = ? WHERE id = ?;', [FALSE, $codigo['id']], ['boolean', 'integer']);
        else
            return array();

        $conn->commit();

        return $codigo;
    }
}
