<?php
namespace App\Model\Table;

use App\Model\Entity\Usuario;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Usuarios Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Tipos
 * @property \Cake\ORM\Association\BelongsTo $Grupos
 * @property \Cake\ORM\Association\BelongsTo $Secoes
 * @property \Cake\ORM\Association\BelongsTo $Status
 * @property \Cake\ORM\Association\HasMany $Auditorias
 */
class UsuariosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('usuarios');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Tipos', [
            'foreignKey' => 'tipo_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Grupos', [
            'foreignKey' => 'grupo_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Secoes', [
            'className' => 'Secoes',
            'foreignKey' => 'secao_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('SecaoTrabalho', [
            'className' => 'Secoes',
            'foreignKey' => 'secao_trabalho_id',
            'propertyName' => 'secao_trabalho'
        ]);
        $this->belongsTo('Status', [
            'foreignKey' => 'status_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Auditorias', [
            'foreignKey' => 'usuario_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('cpf', 'create')
            ->notEmpty('cpf')
            ->add('cpf', 'unique', ['rule' => 'validateUnique', 'provider' => 'table', 'message' => 'Este CPF já está cadastrado']);

        $validator->add('cpf', 'validar_cpf', [
            'rule' => function ($value) {
                return $this->validarCpf ($value);
            },
            'message' => 'CPF inválido! Verifique se está digitando corretamente (somente números)'
        ]);    
        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->allowEmpty('senha');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['tipo_id'], 'Tipos'));
        $rules->add($rules->existsIn(['grupo_id'], 'Grupos'));
        $rules->add($rules->existsIn(['secao_id'], 'Secoes'));
        $rules->add($rules->existsIn(['secao_trabalho_id'], 'Secoes'));
        $rules->add($rules->existsIn(['status_id'], 'Status'));
        return $rules;
    }

    /*
     * Validar CPF
     */
    public function validarCpf ($data) {
        if(strlen($data) != 11)
            return false;
            
        $sum = 0;
        for($i = 0, $j = 10; $i < 9; $i++, $j--)
            $sum += $j*$data[$i];           
        
        $r = $sum % 11;
        
        if($r < 2)
        {
            if($data[9] != 0)
                return false;
        }
        else
        {
            if($data[9] != (11 - $r))
                return false;
        }
            
        $sum = 0;
        for($i = 0, $j = 11; $i < 10; $i++, $j--)
            $sum += $j*$data[$i];
                
        $r = $sum % 11;
        
        if($r < 2)
        {
            if($data[10] != 0)
                return false;
        }
        else
        {
            if($data[10] != (11 - $r))
                return false;
        }       
        
        return true;
    }

    /**
     * Dado uma secao_id verifica se existe algum usuário com secao_id igual e que esteja com status LIBERADO PARA VOTACAO, em caso negativo retorna TRUE
     */
    public function possoLiberarEleitorOuFecharSecao ($secao_id = null) {
        if ($secao_id) {
            // 2 => LIBERADO PARA VOTAR
            $count = $this->find()->where(['secao_id' => $secao_id, 'status_id' => 2])->count();

            if ($count == 0)
                return true;
        }

        return false;
    }
}
