<?php
namespace App\Model\Table;

use App\Model\Entity\Campus;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Campi Model
 *
 * @property \Cake\ORM\Association\HasMany $Candidatos
 * @property \Cake\ORM\Association\HasMany $Secoes
 */
class CampiTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('campi');
        $this->displayField('nome');
        $this->primaryKey('id');

        $this->hasMany('Candidatos', [
            'foreignKey' => 'campus_id'
        ]);
        $this->hasMany('Secoes', [
            'foreignKey' => 'campus_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        return $validator;
    }
}
