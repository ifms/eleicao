<?php
namespace App\Model\Table;

use App\Model\Entity\Secao;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Secoes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Campi
 * @property \Cake\ORM\Association\HasMany $Auditorias
 * @property \Cake\ORM\Association\HasMany $Usuarios
 * @property \Cake\ORM\Association\HasMany $Votos
 */
class SecoesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('secoes');
        $this->displayField('nome');
        $this->primaryKey('id');

        $this->belongsTo('Campi', [
            'foreignKey' => 'campus_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Auditorias', [
            'foreignKey' => 'secao_id'
        ]);
        $this->hasMany('Usuarios', [
            'foreignKey' => 'secao_id'
        ]);
        $this->hasMany('Votos', [
            'foreignKey' => 'secao_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['campus_id'], 'Campi'));
        return $rules;
    }
}
