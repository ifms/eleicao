<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Voto Entity.
 *
 * @property int $id
 * @property int $candidato_id
 * @property \App\Model\Entity\Candidato $candidato
 * @property int $secao_id
 * @property \App\Model\Entity\Secao $secao
 * @property \Cake\I18n\Time $registro
 * @property bool $branco
 * @property bool $nulo
 */
class Voto extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
