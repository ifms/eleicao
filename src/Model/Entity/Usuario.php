<?php
namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * Usuario Entity.
 *
 * @property int $id
 * @property string $cpf
 * @property string $nome
 * @property string $senha
 * @property int $tipo_id
 * @property \App\Model\Entity\Tipo $tipo
 * @property int $grupo_id
 * @property \App\Model\Entity\Grupo $grupo
 * @property int $secao_id
 * @property \App\Model\Entity\Secao $secao
 * @property int $status_id
 * @property \App\Model\Entity\Status $status
 * @property \App\Model\Entity\Auditoria[] $auditorias
 */
class Usuario extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    protected function _setSenha($password)
    {
        return (new DefaultPasswordHasher)->hash($password);
    }
}
