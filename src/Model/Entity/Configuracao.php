<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * Configuracao Entity.
 *
 * @property int $id
 * @property string $chave
 * @property string $valor
 */
class Configuracao extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    public function le ($chave = null)
    {
        $configuracoes = TableRegistry::get('Configuracoes');
        if ($chave AND $configuracoes->exists(['chave' => $chave]))
        {
            $config = $configuracoes->find('all', [
                'conditions' => ['Configuracoes.chave' => $chave]
            ]);

            if ($config->count() > 0)
                return $config->first()->toArray()['valor'];
        }
        return null;
    }
}
