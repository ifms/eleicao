<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AuditoriasFixture
 *
 */
class AuditoriasFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 10, 'autoIncrement' => true, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null],
        'acao' => ['type' => 'string', 'length' => 1000, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'fixed' => null],
        'registro' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null],
        'usuario_id' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'secao_id' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'secoes_fk' => ['type' => 'foreign', 'columns' => ['secao_id', 'secao_id', 'secao_id', 'secao_id'], 'references' => ['secoes', '1' => ['id', 'id', 'id', 'id']], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'usuarios_fk' => ['type' => 'foreign', 'columns' => ['usuario_id'], 'references' => ['usuarios', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'acao' => 'Lorem ipsum dolor sit amet',
            'registro' => 1446583001,
            'usuario_id' => 1,
            'secao_id' => 1
        ],
    ];
}
