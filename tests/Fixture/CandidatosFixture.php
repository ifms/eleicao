<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CandidatosFixture
 *
 */
class CandidatosFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 10, 'autoIncrement' => true, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null],
        'nome' => ['type' => 'string', 'length' => 255, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'fixed' => null],
        'campus_id' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'tipo_id' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'candidatos_uk' => ['type' => 'unique', 'columns' => ['nome', 'campus_id'], 'length' => []],
            'campi_fk' => ['type' => 'foreign', 'columns' => ['campus_id'], 'references' => ['campi', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'tipos_fk' => ['type' => 'foreign', 'columns' => ['tipo_id', 'tipo_id', 'tipo_id', 'tipo_id'], 'references' => ['tipos', '1' => ['id', 'id', 'id', 'id']], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'nome' => 'Lorem ipsum dolor sit amet',
            'campus_id' => 1,
            'tipo_id' => 1
        ],
    ];
}
