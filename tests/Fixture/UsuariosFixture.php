<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * UsuariosFixture
 *
 */
class UsuariosFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 10, 'autoIncrement' => true, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null],
        'cpf' => ['type' => 'string', 'length' => 11, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'fixed' => null],
        'nome' => ['type' => 'string', 'length' => 255, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'fixed' => null],
        'senha' => ['type' => 'string', 'length' => 1000, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null, 'fixed' => null],
        'tipo_id' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'grupo_id' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'secao_id' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'status_id' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'usuarios_cpf_uk' => ['type' => 'unique', 'columns' => ['cpf'], 'length' => []],
            'grupos_fk' => ['type' => 'foreign', 'columns' => ['grupo_id'], 'references' => ['grupos', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'secoes_fk' => ['type' => 'foreign', 'columns' => ['secao_id', 'secao_id', 'secao_id', 'secao_id'], 'references' => ['secoes', '1' => ['id', 'id', 'id', 'id']], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'status_fk' => ['type' => 'foreign', 'columns' => ['status_id'], 'references' => ['status', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'tipos_fk' => ['type' => 'foreign', 'columns' => ['tipo_id', 'tipo_id', 'tipo_id', 'tipo_id'], 'references' => ['tipos', '1' => ['id', 'id', 'id', 'id']], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'cpf' => 'Lorem ips',
            'nome' => 'Lorem ipsum dolor sit amet',
            'senha' => 'Lorem ipsum dolor sit amet',
            'tipo_id' => 1,
            'grupo_id' => 1,
            'secao_id' => 1,
            'status_id' => 1
        ],
    ];
}
