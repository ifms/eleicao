<?php
namespace App\Test\TestCase\Controller;

use App\Controller\PermissoesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\PermissoesController Test Case
 */
class PermissoesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.permissoes',
        'app.grupos',
        'app.usuarios',
        'app.tipos',
        'app.candidatos',
        'app.campi',
        'app.votos',
        'app.secoes',
        'app.auditorias',
        'app.status'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
