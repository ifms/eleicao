<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SecoesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SecoesTable Test Case
 */
class SecoesTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.secoes',
        'app.auditorias',
        'app.usuarios',
        'app.tipos',
        'app.candidatos',
        'app.campi',
        'app.votos',
        'app.grupos',
        'app.permissoes',
        'app.status'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Secoes') ? [] : ['className' => 'App\Model\Table\SecoesTable'];
        $this->Secoes = TableRegistry::get('Secoes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Secoes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
