<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CampiTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CampiTable Test Case
 */
class CampiTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.campi',
        'app.candidatos',
        'app.tipos',
        'app.usuarios',
        'app.grupos',
        'app.permissoes',
        'app.secoes',
        'app.auditorias',
        'app.votos',
        'app.status'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Campi') ? [] : ['className' => 'App\Model\Table\CampiTable'];
        $this->Campi = TableRegistry::get('Campi', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Campi);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
