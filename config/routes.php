<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Core\Plugin;
use Cake\Routing\Router;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 */
Router::defaultRouteClass('DashedRoute');

Router::scope('/', function ($routes) {
    /**
     * Here, we are connecting '/' (base path) to a controller called 'Pages',
     * its action called 'display', and we pass a param to select the view file
     * to use (in this case, src/Template/Pages/home.ctp)...
     */
    $routes->connect('/', ['controller' => 'Pages', 'action' => 'paginaInicial']);
    //$routes->connect('/', ['controller' => 'Usuarios', 'action' => 'loginAd']);

    /**
     * ...and connect the rest of 'Pages' controller's URLs.
     */
    //$routes->connect('/pages/*', ['controller' => 'Pages', 'action' => 'display']);

    $routes->connect('/liberar_eleitor', ['controller' => 'Usuarios', 'action' => 'liberar_eleitor']);
    $routes->connect('/votar', ['controller' => 'Votos', 'action' => 'votar']);
    $routes->connect('/votar-autenticado', ['controller' => 'Votos', 'action' => 'votar-autenticado']);
    $routes->connect('/consultar_protocolo', ['controller' => 'Votos', 'action' => 'consultarProtocolo']);
    $routes->connect('/teste_som', ['controller' => 'Votos', 'action' => 'testeSom']);
    $routes->connect('/contagem_votos', ['controller' => 'Usuarios', 'action' => 'contagemVotos']);
    $routes->connect('/admin', ['controller' => 'Usuarios', 'action' => 'loginAd']);

    $routes->connect('/urna', ['controller' => 'Votos', 'action' => 'votar']);
    $routes->connect('/protocolo', ['controller' => 'Votos', 'action' => 'consultarProtocolo']);
    $routes->connect('/mesario', ['controller' => 'Usuarios', 'action' => 'loginAd']);

    /**
     * Connect catchall routes for all controllers.
     *
     * Using the argument `DashedRoute`, the `fallbacks` method is a shortcut for
     *    `$routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'DashedRoute']);`
     *    `$routes->connect('/:controller/:action/*', [], ['routeClass' => 'DashedRoute']);`
     *
     * Any route class can be used with this method, such as:
     * - DashedRoute
     * - InflectedRoute
     * - Route
     * - Or your own route class
     *
     * You can remove these routes once you've connected the
     * routes you want in your application.
     */
    $routes->fallbacks('DashedRoute');
});

/**
 * Load all plugin routes.  See the Plugin documentation on
 * how to customize the loading of plugin routes.
 */
Plugin::routes();
