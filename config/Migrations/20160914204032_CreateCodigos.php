<?php
use Migrations\AbstractMigration;

class CreateCodigos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('codigos');
        $table->addColumn('codigo', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);
        $table->addColumn('dv', 'string', [
            'default' => null,
            'limit' => 1,
            'null' => false,
        ]);
        $table->addColumn('livre', 'boolean', [
            'default' => true,
            'null' => false,
        ]);
        $table->addIndex('codigo',
            ['unique' => true]
        );
        $table->create();
    }
}
