<?php
use Migrations\AbstractMigration;

class AddTipoToSecoes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('secoes');
        $table->addColumn('tipo', 'string', [
            'default' => 'sistema',
            'limit' => 255,
            'null' => false,
        ]);
        $table->update();
    }
}
