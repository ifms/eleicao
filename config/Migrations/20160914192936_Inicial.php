<?php
use Migrations\AbstractMigration;

class Inicial extends AbstractMigration
{
    public function up()
    {

        $this->table('auditorias')
            ->addColumn('acao', 'string', [
                'default' => null,
                'limit' => 1000,
                'null' => false,
            ])
            ->addColumn('usuario_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('secao_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('registro', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'secao_id',
                ]
            )
            ->addIndex(
                [
                    'usuario_id',
                ]
            )
            ->create();

        $this->table('campi')
            ->addColumn('nome', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->create();

        $this->table('candidatos')
            ->addColumn('nome', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('campus_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('tipo_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addIndex(
                [
                    'nome',
                    'campus_id',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'campus_id',
                ]
            )
            ->addIndex(
                [
                    'tipo_id',
                ]
            )
            ->create();

        $this->table('configuracoes')
            ->addColumn('chave', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('valor', 'string', [
                'default' => null,
                'limit' => 500,
                'null' => false,
            ])
            ->create();

        $this->table('grupos')
            ->addColumn('nome', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->create();

        $this->table('permissoes')
            ->addColumn('grupo_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('controller', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('action', 'string', [
                'default' => null,
                'limit' => 1000,
                'null' => false,
            ])
            ->addIndex(
                [
                    'grupo_id',
                    'controller',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'grupo_id',
                ]
            )
            ->create();

        $this->table('secoes')
            ->addColumn('nome', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('campus_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('abertura', 'timestamp', [
                'comment' => 'Registro do horário em que houve a abertura da Seção.',
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('fechamento', 'timestamp', [
                'comment' => 'Registro do horário em que houve o fechamento da Seção.',
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('abertura_autorizada', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('fechamento_autorizado', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addIndex(
                [
                    'campus_id',
                ]
            )
            ->create();

        $this->table('status')
            ->addColumn('nome', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->create();

        $this->table('tipos')
            ->addColumn('nome', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->create();

        $this->table('usuarios')
            ->addColumn('cpf', 'string', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('nome', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('senha', 'string', [
                'default' => null,
                'limit' => 1000,
                'null' => true,
            ])
            ->addColumn('tipo_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('grupo_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('secao_id', 'integer', [
                'comment' => 'Seção onde o usuário irá votar.',
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('status_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('secao_trabalho_id', 'integer', [
                'comment' => 'Seção onde o usuário irá trabalhar',
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addIndex(
                [
                    'cpf',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'grupo_id',
                ]
            )
            ->addIndex(
                [
                    'secao_id',
                ]
            )
            ->addIndex(
                [
                    'secao_trabalho_id',
                ]
            )
            ->addIndex(
                [
                    'status_id',
                ]
            )
            ->addIndex(
                [
                    'tipo_id',
                ]
            )
            ->create();

        $this->table('votos')
            ->addColumn('candidato_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('secao_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('registro', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('branco', 'boolean', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('nulo', 'boolean', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->create();

        $this->table('auditorias')
            ->addForeignKey(
                'secao_id',
                'secoes',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->addForeignKey(
                'usuario_id',
                'usuarios',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->update();

        $this->table('candidatos')
            ->addForeignKey(
                'campus_id',
                'campi',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->addForeignKey(
                'tipo_id',
                'tipos',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->update();

        $this->table('permissoes')
            ->addForeignKey(
                'grupo_id',
                'grupos',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->update();

        $this->table('secoes')
            ->addForeignKey(
                'campus_id',
                'campi',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->update();

        $this->table('usuarios')
            ->addForeignKey(
                'grupo_id',
                'grupos',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->addForeignKey(
                'secao_id',
                'secoes',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->addForeignKey(
                'secao_trabalho_id',
                'secoes',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->addForeignKey(
                'status_id',
                'status',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->addForeignKey(
                'tipo_id',
                'tipos',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->update();
    }

    public function down()
    {
        $this->table('auditorias')
            ->dropForeignKey(
                'secao_id'
            )
            ->dropForeignKey(
                'usuario_id'
            );

        $this->table('candidatos')
            ->dropForeignKey(
                'campus_id'
            )
            ->dropForeignKey(
                'tipo_id'
            );

        $this->table('permissoes')
            ->dropForeignKey(
                'grupo_id'
            );

        $this->table('secoes')
            ->dropForeignKey(
                'campus_id'
            );

        $this->table('usuarios')
            ->dropForeignKey(
                'grupo_id'
            )
            ->dropForeignKey(
                'secao_id'
            )
            ->dropForeignKey(
                'secao_trabalho_id'
            )
            ->dropForeignKey(
                'status_id'
            )
            ->dropForeignKey(
                'tipo_id'
            );

        $this->dropTable('auditorias');
        $this->dropTable('campi');
        $this->dropTable('candidatos');
        $this->dropTable('configuracoes');
        $this->dropTable('grupos');
        $this->dropTable('permissoes');
        $this->dropTable('secoes');
        $this->dropTable('status');
        $this->dropTable('tipos');
        $this->dropTable('usuarios');
        $this->dropTable('votos');
    }
}
